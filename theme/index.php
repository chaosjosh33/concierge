<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Insurance Concierge</title>
		<!-- Bootstrap Core CSS -->
		<link href="<?php echo $settings['base_uri'];?>theme/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="<?php echo $settings['base_uri'];?>theme/css/modern-business.css" rel="stylesheet">
		<!-- Custom Fonts -->
		<link href="<?php echo $settings['base_uri'];?>theme/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo $settings['base_uri'];?>theme/css/bootstrap-formhelpers-min.css" media="screen">
		<link rel="stylesheet" href="<?php echo $settings['base_uri'];?>theme/css/bootstrapValidator-min.css"/>
		<script>
			var serialize = function(obj) {
				var str = [];
				for(var p in obj)
					if (obj.hasOwnProperty(p)) {
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					}
				return str.join("&");
			};
			var base_uri = "<?php echo $settings['base_uri'];?>";
			var api_uri = "<?php echo $settings['api_uri'];?>";
			var userData = <?php echo json_encode($userData);?>;
		</script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	</head>
	<body>
		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="<?php echo $settings['base_uri'];?>"><img class="img-responsive" width="400px" style="padding:20px" src="<?php echo $settings['base_uri'];?>theme/images/logo.png"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a id="buy" data-toggle="modal" data-target="#myModal" _parentid="" href>Purchase</a>
						</li>
							<?php /*?>
						<li>
							<a href="#content/contact">Contact</a>
						</li>
						<?php */?>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<div id="content">
		</div>
		<!-- Page Content -->
		<div class=" footer-overlay">
			<div class="container">
				<!-- Footer -->
				<footer>
					<div class="row">
						<div class="col-lg-12">
							<p>Copyright &copy; Insurance Concierge 2016</p>
						</div>
					</div>
				</footer>
			</div>
		</div>
		<!-- /.container -->
		<!-- jQuery -->
		<link rel="icon" href="<?php echo $settings['base_uri'];?>theme/images/favicon.ico?ver=4" />
		<script src="<?php echo $settings['base_uri'];?>theme/js/jquery.js"></script>
		<script src="<?php echo $settings['base_uri'];?>theme/js/vendor/jquery-ui/jquery-ui.min.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="<?php echo $settings['base_uri'];?>theme/js/bootstrap.min.js"></script>
		<script src="<?php echo $settings['base_uri'];?>theme/js/routie.min.js"></script>
		<script src="<?php echo $settings['base_uri'];?>theme/js/routes.js"></script>
		<script src="https://js.stripe.com/v2/"></script>
		<script src="<?php echo $settings['base_uri'];?>theme/js/bootstrap-formhelpers-min.js"></script>
		<script src="<?php echo $settings['base_uri'];?>theme/js/bootstrapValidator-min.js"></script>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content" id="modalContent">
				</div>
			</div>
		</div>
		<!-- Script to Activate the Carousel -->
		<script>
			function ajaxLoad(attrUrl,attrDiv) {
				var attrUrl = base_uri+api_uri+"/"+attrUrl;
				if (typeof attrUrl !== typeof undefined && attrUrl !== false) {
					var attrDiv = attrDiv;
					if (typeof attrDiv !== typeof undefined && attrDiv !== false) {
						if ($("#" + attrDiv).length != 0) {
							$.ajax({
								url: attrUrl,
								cache: false,
								success: function (html) {
									$("#" + attrDiv).html(html);
									console.log("Load was performed.");
									return true;
								}
							});
						}
					}
				}
				return false;
			}
			$(function(){
				$('#buy').on('click', function(){
					$( "#modalContent" ).load( base_uri + "api/products/productList.php", function(){
					});
				});
			});
		</script>
	</body>
</html>
