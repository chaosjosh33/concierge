 <ul class="nav" id="side-menu">
            <li class="active">
                <a href="#content/dashboard"> <span class="nav-label">Dashboard</span> <span class="label label-success pull-right">v.1</span> </a>
            </li>
             <li class="active">
                <a href="#content/news"> <span class="nav-label">News</span> </a>
            </li>
        
            <li>
                <a href="#"><span class="nav-label">Leads</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="#content/leads">All Leads</a></li>
                    <li><a href="#content/leads/new">Create</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><span class="nav-label">Clients</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="#content/clients/">All Clients</a></li>
                    <li><a href="#content/clients/new">New Clients</a></li>
                    <li><a href="#content/clients/followup">Follow Up</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><span class="nav-label">Policies</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="#content/policies/">All Policies</a></li>
                    <li><a href="#content/policies/new">New Policies</a></li>
                    <li><a href="#content/policies/expiring">Expiring Policies</a></li>
                    <li><a href="#content/policies/submit">Submit Today</a></li>
                    <li><a href="#content/policies/pastdue">Past Due</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><span class="nav-label">Reports</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                     <li><a onClick="event.preventDefault();ajaxLoad('reports/','content')">All Reports</a></li>
                    <li><a href="#content/reports/saved">Saved Reports</a></li>
                    <li><a href="#content/reports/create">Create Report</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><span class="nav-label">Settings</span><span class="fa arrow"></span> </a>
                <ul class="nav nav-second-level">
                    <li><a href="#content/settings/agency">Agency Settings</a></li>
                    <li><a href="#content/settings/users">Users</a></li>
                    <li><a href="#content/settings/teams">Teams</a></li>
                    <li><a href="#content/settings/smsTemplates">SMS Templates</a></li>

                </ul>
            </li>
           
        </ul>