routie({
    
    ':div/:apiCall': function(div,apiCall) {
         $('#'+div).load(base_uri +''+api_uri+'/'+apiCall+'');
    },
    ':div/:apiCall/:filter1': function(div,apiCall,filter1) {
         $('#'+div).load(base_uri +''+api_uri+'/'+apiCall+'/'+filter1);
    },
    ':div/:apiCall/:filter1/:filter2': function(div,apiCall,filter1,filter2) {
         $('#'+div).load(base_uri +''+api_uri+'/'+apiCall+'/'+filter1+'/'+filter2);
    },
    ':div/:apiCall/:filter1/:filter2/:filter3': function(div,apiCall,filter1,filter2,filter3) {
         $('#'+div).load(base_uri +''+api_uri+'/'+apiCall+'/'+filter1+'/'+filter2+'/'+filter3);
    },
    ':div/:apiCall/:filter1/:filter2/:filter3/:filter4': function(div,apiCall,filter1,filter2,filter3,filter4) {
         $('#'+div).load(base_uri +''+api_uri+'/'+apiCall+'/'+filter1+'/'+filter2+'/'+filter3+'/'+filter4);
    },
    ':div/:apiCall/:filter1/:filter2/:filter3/:filter4/:filter5': function(div,apiCall,filter1,filter2,filter3,filter4,filter5) {
         $('#'+div).load(base_uri +''+api_uri+'/'+apiCall+'/'+filter1+'/'+filter2+'/'+filter3+'/'+filter4+'/'+filter5);
    },
    
 
    '*': function() {
        console.log(base_uri +''+api_uri+ '/landing');
        $('#content').load(base_uri +''+api_uri+ '/landing');
    },
});