<?php
date_default_timezone_set('America/Los_Angeles');
setlocale(LC_MONETARY,'en_US.UTF-8');

// Debuger array to catch exceptions in
$debugger = array();

// Set default settings
$settings = array();
$settings['site_name'] = "ThingCRM";
$settings['domain'] = "insurancecrm.net";
$settings['api_dir'] = "api/"; // Directory your App files are stored physically with trailing slash
$settings['api_uri'] = "api"; // URI used in browser when calling Apps
$settings['base_uri'] = "/";
$settings['baseUri'] = "/";
$settings['login_page'] = "api/auth/login";
$settings['registration_page'] = "api/auth/signup";
$settings['encrypted_key'] = "DNKLMMQWFNJKNLWEmnklmmklwmklwm@KM#KLM";
$settings['encryptionIV'] = "Dgdsg$*&WwegwehweEgwgw4367";
$settings['password_salt'] = "shrer$%@Gregwewegwehweh";
$settings['minimum_password_length'] = 6;
$settings['debug_mode'] = "OFF";

// Users
$settings['default_user_status'] = "ACTIVE";
$settings['default_user_approval'] = "N";

// Remote ThingEngine
$settings['apiKey'] = "APIKas32492342359235m23523m5235";
$settings['thingEngineURI'] = "api/";

// DATABASE SERVER
$settings['dbserver'] = "localhost";
$settings['dbuser'] = "root";
$settings['dbpass'] = "Qduki2uJ";
$settings['dbname'] = "concierge";
//$settings['dbuser']  = "tester";
//$settings['dbpass']  = "X5hCDyvERhZjdnZL";
//$settings['dbname']  = "tester";

// THIRD PARTY PLUGINS
//$settings['baseUri']='/crmfrontend/';
$settings['firebaseUrl'] = 'https://sweltering-heat-2490.firebaseio.com/';
$settings['firebaseSecret'] = 'yRvxZFM5vtLlN5uRb6ZA60XkieFrkLzzfsza0nd8';
$settings['mailgun']['apikey'] = 'key-57f89741d21efcdb98245727b4128344';
$settings['mailgun']['domains'] = array('allinsurancecenter.com','exchangeadvisers.com','insurehc.com');
$settings['stripe']['test']['secretkey']='sk_test_a3tXyb2zPM316mFhoTJXbbk5';
$settings['stripe']['test']['publickey']='pk_test_ibkg34nt2Uv9Cj15DcG3Dy10';
$settings['stripe']['live']['secretkey']='sk_live_OWFpTgae8oE9bCKhZMrYqWLt';
$settings['stripe']['live']['publickey']='pk_live_htsuDB4CnSWj98ZFcuug43yh';

// Format Date and Time Displays
$settings['date_format'] = "m/d/Y";
$settings['time_format'] = "h:i a";
$settings['date_time_format'] = "m/d/Y h:i a";
$settings['timestamp_format'] = "m/d/Y H:i:s";

// Store JS used by apps
$settings['js'] = array();

// Store CSS used by apps
$settings['css'] = array();

// Up to 5.6 PHP need to Serialize
define("SETTINGS",serialize($settings));