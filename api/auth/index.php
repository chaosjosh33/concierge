<?php
require_once __DIR__ . '/../init.php';
$thingEngine->slim->config(array(
    'templates.path' => __DIR__
));



$thingEngine->slim->map('/', function () use ($thingEngine,$thingEngineApi, $settings) {
})->via('GET','POST');


$thingEngine->slim->map('/register', function () use ($thingEngine,$thingEngineApi, $settings) {
    $message=false;
    $thingEngine->slim->render('registerView.php', array('settings'=>$settings, 'thingEngineApi'=>$thingEngineApi, 'message'=>$message));
})->via('GET','POST');

$thingEngine->slim->map('/registerSave', function () use ($thingEngine,$thingEngineApi, $settings) {
    $message=false;
    $data=array();
    $userTable = "testUser";
    $user  = new \thingEngine\auth($settings);
    $user->setPersonTable($userTable);
    $results = $user->registerUser($_POST);

    $_SESSION['api']['register']['id'] = $user->id;
    $_SESSION['api']['register']['email'] = $user->emailPrimary;
    
    $response = $thingEngine->slim->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'Thing Engine';
    $response->status(200);
    $response->body(json_encode($results));
})->via('GET','POST');

$thingEngine->slim->map('/registerQuestionsOLD', function () use ($thingEngine,$thingEngineApi, $settings) {
    $message=false;
    $thingEngine->slim->render('registerQuestionsView.php', array('settings'=>$settings, 'thingEngineApi'=>$thingEngineApi, 'message'=>$message));
})->via('GET','POST');

$thingEngine->slim->map('/registerQuestions', function () use ($thingEngine, $settings){
    $insuranceAgent =  new \thingEngine\auth($settings);
   //  $thingEngine->slim->render('registerQuestionsView.php', array('settings'=>$settings));
    debug($_SESSION);
    echo "HERE";
})->via('GET','POST');

$thingEngine->slim->map('/login', function () use ($thingEngine, $thingEngineApi, $settings) {
    if(!empty($_POST)){
        $insuranceAgent =  new \thingEngine\auth($settings);
        $results = $insuranceAgent->logginUser($_POST['email'], $_POST['password']);
        if(($results['result'] == "Success") && (!empty($_SESSION['api']['user']['id']))){
             $thingEngine->slim->redirect($settings['base_uri']);
        } else {
             $thingEngine->slim->render('loginView.php', array('settings'=>$settings, 'thingEngineApi'=>$thingEngineApi, 'message'=>$results['message']));   
        }
    }else{
        $thingEngine->slim->render('loginView.php', array('settings'=>$settings, 'thingEngineApi'=>$thingEngineApi));
    }
})->via('GET','POST');


$thingEngine->slim->map('/logout', function () use ($thingEngine, $thingEngineApi, $settings) {
    $_SESSION['api']['user'] = array();
    $thingEngine->slim->redirect($settings['base_uri']);
})->via('GET','POST');


$thingEngine->slim->map('/checkEmail', function () use ($thingEngine, $thingEngineApi, $settings) {
    $email=$_POST['email'];
    $user  = new \thingEngine\auth($settings);
	$insuranceAgent  = \RedBeanPHP\Facade::findOne( $user->userTable, " emailPrimary = '".strtolower(trim($email))."' " );
    if(!empty($insuranceAgent)){
        echo json_encode('Email is already in use.');
    }else{
        echo 'true';
    }
})->via('GET','POST');



$thingEngine->slim->run();