<?php
$_SESSION['test'] = "here";
?>
    <html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Page title -->
    <title>ThingCRM</title>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->
    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>theme/js/vendor/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>theme/js/vendor/metisMenu/dist/metisMenu.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>theme/js/vendor/animate.css/animate.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>theme/js/vendor/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $settings['base_uri'];?>theme/js/vendor/sweetalert/lib/sweet-alert.css" />
    <link rel="stylesheet" href="<?php echo $settings['base_uri'];?>theme/js/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />
    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>theme/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>theme/fonts/pe-icon-7-stroke/css/helper.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>theme/styles/style.css">
    <style type="text/css">
        .jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }
        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }
    </style>
</head>
<body class="blank">
    <!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    <div class="color-line"></div>
    <div class="register-container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center m-b-md">
                    <h3>Registration</h3>
                    <small>Please Fill Out All Fields.</small>
                    <?php
						if(!empty($message)){
						?>
                        <p class="text-danger">
                            <?php echo $message;?>
                        </p>
                        <?php
						}
	?>
                </div>
                <div class="hpanel">
                    <div class="panel-body">
                        <form method="post" id="registerForm">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label for="firstname">First Name</label>
                                    <input type="text" id="firstname" class="form-control" name="firstname" required="true" aria-required="true" placeholder="First Name">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="lastname">Last Name</label>
                                    <input type="text" id="lastname" class="form-control" name="lastname" required="true" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label for="password">Password</label>
                                    <input type="password" id="password" class="form-control" name="password">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="passwordConf">Repeat Password</label>
                                    <input type="password" id="passwordConf" class="form-control" name="passwordConf">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label for="email">Email Address</label>
                                    <input type="email" id="email" class="form-control" name="email" placeholder="name@domain.com">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="phone">Phone</label>
                                    <input type="phone" id="phone" class="form-control" name="phone" placeholder="(555) 555-5555">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-12">
                                    <label for="address">Address</label>
                                    <input type="address" id="address" class="form-control" name="address" placeholder="Street Address">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="city">City</label>
                                    <input type="text" id="city" class="form-control" name="city" placeholder="City">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="phone">State</label>
                                    <select id="state" name="state" class="form-control" required="1" placeholder="State" >
                                        <option selected="selected" value="">State</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="zipcode">Zip</label>
                                    <input type="text" id="zipcode" class="form-control" name="zipCode" placeholder="Zip Code">
                                </div>
                            </div>
                            <div class="row" style="display:none">
                                <div class="form-group col-lg-6">
                                    <label for="licensedStates">Licensed States</label> <br>(Hold CTRL button to select multiple)
                                    <select id="licensedStates" name="licensedStates[]" class="form-control" multiple="1" required="1" placeholder="State License" tabindex="10" style="min-height: 100px;">
                                        <option value="NONE">I Am Not Currently Licensed</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="licenses">Type of Licenses</label> <br>(Hold CTRL button to select multiple)
                                    <select id="licenses" name="licenses[]" class="form-control" multiple="1"  placeholder="License Types" tabindex="11" style="min-height: 100px;">
                                        <option value="Health">HEALTH</option>
                                        <option value="Life">LIFE</option>
                                        <option value="P&amp;C">P&amp;C</option>
                                        <option value="Financial">FINANCIAL</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="row" style="padding-top:20px; padding-bottom: 20px">
                                <div class="form-group col-sm-6 text-center">
                                    <label for="licensed">I Am A Licensed Agent</label>
                                    <br>
                                    <div class="icheckbox_square-green" style="position: relative;">
                                        <input type="checkbox" class="i-checks" style="position: absolute; opacity: 0;" id="licensed" name="licensed">
                                    </div>
                                    I am currently licensed to sell insurance.
                                    
                                </div>
                     
                                <div class="form-group col-sm-6  text-center">
                                    <label for="agreeToTerms">Agree to Terms of Service</label>
                                    <br>
                                    <div class="icheckbox_square-green" style="position: relative;">
                                        <input type="checkbox" class="i-checks" style="position: absolute; opacity: 0;" id="agreeToTerms" name="agreeToTerms">
                                    </div>
                                    Yes, I agree to the Terms of Service. 
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">Register</button>
                                    <a href="<?php echo $settings['baseUri'];?>api/auth/registerQuestionsOLD" class="btn btn-default">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <br> 2016 Copyright Insurance CRM
                </div>
            </div>
        </div>
        <!-- Vendor scripts -->
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/iCheck/icheck.min.js"></script>
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/sparkline/index.js"></script>
        <script src="<?php echo $settings['base_uri'];?>theme/js/vendor/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="<?php echo $settings['base_uri'];?>theme/js/vendor/toastr/build/toastr.min.js"></script>
        <script src="<?php echo $settings['baseUri'];?>theme/js/vendor/jquery-validation/jquery.validate.min.js"></script>
        <!-- App scripts -->
        <script src="<?php echo $settings['baseUri'];?>theme/js/homer.js"></script>
        <script type="text/javascript">
            $(function () {
                $('#registerForm2').on('submit', function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    console.log("Submitting...");
                    return false;
                    $.post('<?php echo $settings['baseUri'];?>api/auth/register', $(this).serialize(),
                        function (response) {
                            console.log(response);
                            window.location = '<?php echo $settings['baseUri'];?>api/auth/login';
                        });
                });
                $('#registerForm').validate({
                    rules: {
                        password: {
                            required: true,
                            minlength: 6,
                        },
                        passwordConf: {
                            required: true,
                            minlength: 6,
                            equalTo: '#password'
                        },
                        firstname: {
                            required: true,
                        },
                        lastname: {
                            required: true,
                        },
                        phone: {
                            required: true,
                        },
                         address: {
                            required: true,
                        },
                         city: {
                            required: true,
                        },
                         state: {
                            required: true,
                        },
                         zipCode: {
                            required: true,
                        },
                        licensedStates: {
                            required: true,
                        },
                        
                        email: {
                            required: true,
                            email: true,
                            remote: {
                                url: "checkEmail",
                                type: "post",
                            }
                        },
                        agreeToTerms: {
                            required: true,
                        },
                    },
                    messages: {
                        passwordConf: {
                            equalTo: 'Passwords Must Match'
                        },
                    },
                    submitHandler: function (form) {
                       
                        
                        $.post('<?php echo $settings['baseUri'];?>api/auth/registerSave', $('#registerForm').serialize(),
                        function (response) {
                            console.log(response);
                           // return false;
                            if(response['result'] == "Error"){
                                 swal({
                                    title: "Error.",
                                    text: response['message'],
                                    type: "error"
                                });
                            } else {
                                //window.location = '<?php echo $settings['baseUri'];?>api/auth/registerQuestions';
                                window.location = '<?php echo $settings['baseUri'];?>api/auth/login';
                            }
                            
                        });
                        
                    },
                    errorPlacement: function (error, element) {
                        $(element)
                            .closest("form")
                            .find("label[for='" + element.attr("id") + "']")
                            .append(error);
                    },
                    errorElement: "span",
                });
            });
        </script>
    </body>
    </html>