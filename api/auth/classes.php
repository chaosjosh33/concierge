<?php
namespace thingEngine;
class auth  extends \thingEngine\person {
	var $password;
	var $agreeToTerms;
	var $licensed;
	var $approved;
	var $status;
	var $permissionLevel;
	var $userTable;
	var $address = array();
	function __construct($settings = false){
		$this->userTable = "insuranceAgents";
		$this->settings = $settings;
		$this->extensionInfo['name'] = "Authorization Extension";
		$this->extensionInfo['description'] = "Basic Authorization Structure";
		$this->extensionInfo['author'] = "ThingEngine";
		$this->extensionInfo['owner'] = "thingengine";
		$this->extensionInfo['price'] = "0.00";
		$this->extensionInfo['version'] = "1.0";
		$this->extensionInfo['dateCreated'] = "12/01/2015";
	}
	function userloggedin($cookieValue = false){
		// Check by Cookie
		$user_id = 0;
		if((empty($_SESSION['api']['user']['id'])) || (trim($_SESSION['api']['user']['id']) < 1)) {
			if ( $cookieValue ) {
				$user_id = $cookieValue;
				return $user_id;
			} else {
				return 0;
			}
		} else {
			$user_id = $_SESSION['api']['user']['id'];
			return $user_id;
		}
	}
	function logginUser($email,$password){
		if((!empty($this->settings['minimum_password_length'])) && (strlen($password) < $this->settings['minimum_password_length'])){
			$results['code'] = "AUTH0010"; 
			$results['result'] = "Error"; 
			$results['message'] = "the password field must be at least ".$this->settings['minimum_password_length']." characters long"; 
			$results['data'] = array();
			return $results;
		}    
		$insuranceAgent  = \RedBeanPHP\Facade::findOne( $this->userTable, " emailPrimary = '".strtolower(trim($email))."' " );
		if(!empty($insuranceAgent)){
			$options = [
				'cost' => 10,
				'salt' => $this->settings['password_salt']
			];
			$value =  password_hash($password, PASSWORD_BCRYPT, $options);
			if((!empty($insuranceAgent->password)) && ($value == $insuranceAgent->password)){
				unset($_SESSION['api']['user']);
				$_SESSION['api']['user'] = array();
				$results['code'] = "AUTH0001"; 
				$results['result'] = "Success"; 
				$results['message'] = "User is logged In"; 
				foreach($insuranceAgent as $key=>$var){
					if($key <> "password"){
						$results['data'][$key] = $var;
						$_SESSION['api']['user'][$key] = $var;
					}
				}
			} else {
				$results['code'] = "AUTH0015"; 
				$results['result'] = "Error"; 
				$results['message'] = "Could not match that email and password"; 
				$results['data'] = array();   
			}
		} else {
			$results['code'] = "AUTH0020"; 
			$results['result'] = "Error"; 
			$results['message'] = "Could not match that email and password"; 
			$results['data'] = array();   
		}
		return $results;
	}
	function saveUser(){
		$this->saveData($this->userTable);
	}
	function registerUser($postVars){
		$results = array();
		if(!empty($postVars)){
			if((!empty($postVars['licensed'])) && (strtoupper($postVars['licensed'])=='ON')){
				$postVars['licensed']='Y';
			}else{
				$postVars['licensed']='N';
			}
			if((!empty($postVars['agreeToTerms'])) && (strtoupper($postVars['agreeToTerms'])=='ON')){
				$postVars['agreeToTerms']='Y';
			}else{
				$postVars['agreeToTerms']='N';
			}
			if(!empty($postVars['approved'])){
				$this->setVarToUpper("approved",$postVars['approved']);
			}else{
				$this->setVar("approved", $this->settings['default_user_approval']);
			}
			if(!empty($postVars['status'])){
				$this->setVarToUpper("status",$postVars['status']);
			}else{
				$this->setVar("status", $this->settings['default_user_status']);
			}
			$insuranceAgent = \RedBeanPHP\Facade::getAll("select id from ".$this->userTable." where primaryEmail ='".strtolower(trim($postVars["email"]))."' limit 1");
			if(empty($insuranceAgent[0]['id'])){
				$thingEngine = new \thingEngine\engine();
				$this->setReferenceId($thingEngine->getRandomId());
				$this->setVarToUpper("firstName", $postVars["firstname"]);
				$this->setVarToUpper("lastName", $postVars["lastname"]);
				$this->setVarToUpper("licensed", $postVars["licensed"]);
				$this->setVarToUpper("agreeToTerms", $postVars["agreeToTerms"]);
				$this->setVarToPassword("password",$postVars["password"]);
				$this->setVarToEmail("emailPrimary", $postVars["email"]);
				$this->address[0]  = new \thingEngine\address();
				$this->address[0]->street = strtoupper($postVars['address']);
				$this->address[0]->zipCode = $postVars['zipCode'];
				$this->address[0]->state = $postVars['state'];
				$this->address[0]->city = strtoupper($postVars['city']);
				$this->saveUser();
				$results['code'] = "REGISTER10001"; 
				$results['result'] = "Success"; 
				$results['message'] = "User has been created"; 
				$results['data'] = array();    
			} else {
				$results['code'] = "REGISTER10010"; 
				$results['result'] = "Error"; 
				$results['message'] = "The email provided already exists in system"; 
				$results['data'] = array();
			}
		} else {
			$results['code'] = "REGISTER10005"; 
			$results['result'] = "Error"; 
			$results['message'] = "No User Information Was Submitted."; 
			$results['data'] = array();
		}
		return $results;   
	}
	function loadUsers($params=false){
		if(!empty($params['id'])){
			$obj = \RedBeanPHP\Facade::load('insuranceAgents',$params['id']);
			if(!empty($obj)){
				foreach( $obj as $key=>$var ) {
					$this->$key = $var;
				}
				return $this;
			}
		}else{
			$userList = array();
			$obj = \RedBeanPHP\Facade::find('insuranceAgents','status = :status LIMIT :limit OFFSET :offset ',[':offset' => $params['offset'],':limit' => $params['limit'], ':status' => $params['status']]);
			$i=0;
			if(!empty($obj))
			{
				foreach ($obj as $obj2)
				{
					foreach ($obj2 as $key => $var)
					{
						$userList[$i][$key]=$var;
					}
					$i++;
				}
			}else{
				$userList = 'empty';
			}
			return $userList;
		}
	}
}