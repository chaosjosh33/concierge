<?php

// API group
$thingEngine->slim->group("/".$thingEngine->settings['api_uri'], function () use ($thingEngine) {
    // Auth group
    $thingEngine->slim->group('/auth', function () use ($thingEngine) {
        // Home
        $thingEngine->slim->get('/', function () use ($thingEngine){
            echo "Auth Home";
        });
        $thingEngine->slim->map('/emailExists', function () use ($thingEngine){
            $results = array();
            $registrationError = false;
            $errorcode = 10090;
            $requiredFields = array("email");
            $userTable = "testUser";
            $insuranceAgent = \RedBeanPHP\Facade::getAll("select id from ".$userTable." where emailPrimary ='".strtolower($_REQUEST["email"])."' limit 1");
            if(!empty($insuranceAgent[0]['id'])){
                $results['code'] = $errorcode; 
                $results['result'] = "Error"; 
                $results['message'] = "the email provided already exists in system"; 
                $results['data'] = array();   
                $registrationError = true;
                $errorcode++;
            } else {
                $results['code'] = "10001"; 
                $results['result'] = "Success"; 
                $results['message'] = "Email Does Not Exist In System"; 
                $results['data'] = array();    
            }
            $response = $thingEngine->slim->response();
            $response['Content-Type'] = 'application/json';
            $response['X-Powered-By'] = 'Thing Engine';
            $response->status(200);
            $response->body(json_encode($results));
        })->via('GET','POST'); 
        //
        //
        //
        //
        //
        //
        $thingEngine->slim->map('/register', function () use ($thingEngine){
            $results = array();
            $requiredFields = array("firstname","lastname","email","phone","password","passwordConf","licensed","agreeToTerms");
            $registrationError = false;
            $errorcode = 10070;
            foreach($requiredFields as $key=>$var){
                if(empty($_REQUEST[$var])){
                    $results['code'] = $errorcode; 
                    $results['result'] = "Error"; 
                    $results['message'] = "the field ".$var." not sent"; 
                    $results['data'] = array(
                        "requiredFields" => $requiredFields
                    );
                    $registrationError = true;
                    break;
                }
                $errorcode++;
            }
            if($registrationError === false){
                if($_REQUEST["password"] <> $_REQUEST["passwordConf"]){
                    $results['code'] = $errorcode; 
                    $results['result'] = "Error"; 
                    $results['message'] = "the password fields do not match"; 
                    $results['data'] = array(
                        "requiredFields" => $requiredFields
                    );
                    $registrationError = true;
                    $errorcode++;
                }    
            }
            if($registrationError === false){
                if(strlen($_REQUEST["password"]) < $thingEngine->settings['minimum_password_length']){
                    $results['code'] = $errorcode; 
                    $results['result'] = "Error"; 
                    $results['message'] = "The password field must be at least 6 characters long"; 
                    $results['data'] = array(
                        "requiredFields" => $requiredFields
                    );
                    $registrationError = true;
                    $errorcode++;
                }    
            }
            if($registrationError === false){
                $userTable = "testUser";
                $insuranceAgent = \RedBeanPHP\Facade::getAll("select id from ".$userTable." where emailPrimary ='".strtolower($_REQUEST["email"])."' limit 1");
                if(empty($insuranceAgent[0]['id'])){
                    $options = [
                        'cost' => 10,
                        'salt' => $thingEngine->settings['password_salt']
                    ];
                    $_REQUEST['password'] =  password_hash($_REQUEST['password'], PASSWORD_BCRYPT, $options);
                    $user  = new \thingEngine\auth();
                    $user->setPersonTable($userTable);
                    $user->setReferenceId($thingEngine->getRandomId());
                    $user->setPersonFirstName($_REQUEST["firstname"]);
                    $user->setPersonLastName($_REQUEST["lastname"]);
                    $user->setAuthLicensed($_REQUEST["licensed"]);
                    $user->setAuthAgreeToTerms($_REQUEST["agreeToTerms"]);
                    $user->setAuthPassword($_REQUEST["password"]);
                    $user->setPersonEmailPrimary($_REQUEST["email"]);
                    $user->savePerson();
                    $results['code'] = "10001"; 
                    $results['result'] = "Success"; 
                    $results['message'] = "User has been created"; 
                    $results['data'] = array();    
                } else {
                    $results['code'] = $errorcode; 
                    $results['result'] = "Error"; 
                    $results['message'] = "The email provided already exists in system"; 
                    $results['data'] = array(
                        "requiredFields" => $requiredFields
                    );
                    $registrationError = true;
                    $errorcode++;
                }
            }
            /*
            */
            $response = $thingEngine->slim->response();
            $response['Content-Type'] = 'application/json';
            $response['X-Powered-By'] = 'Thing Engine';
            $response->status(200);
            $response->body(json_encode($results));
        })->via('GET','POST'); 
        //
        //
        //
        //
        //
        //
        $thingEngine->slim->get('/login', function () use ($thingEngine){
            $results = array();
            $registrationError = false;
            $errorcode = 10190;
            $requiredFields = array("email");
            $userTable = "testUser";
            if($registrationError === false){
                if(strlen($_REQUEST["password"]) < $thingEngine->settings['minimum_password_length']){
                    $results['code'] = $errorcode; 
                    $results['result'] = "Error"; 
                    $results['message'] = "the password field must be at least 6 characters long"; 
                    $results['data'] = array(
                        "requiredFields" => $requiredFields
                    );
                    $registrationError = true;
                    $errorcode++;
                }    
            }
            $insuranceAgent = \RedBeanPHP\Facade::getAll("select * from ".$userTable." where emailPrimary ='".strtolower($_REQUEST["email"])."' limit 1");
            if(!empty($insuranceAgent[0]['id'])){
                $options = [
                    'cost' => 10,
                    'salt' => $thingEngine->settings['password_salt']
                ];
                
                $value =  password_hash($_REQUEST['password'], PASSWORD_BCRYPT, $options);
                if((!empty($insuranceAgent[0]['password'])) && ($value == $insuranceAgent[0]['password'])){
                    unset($insuranceAgent[0]['password']);
                    $_SESSION['api']['user'] = $insuranceAgent[0];
                    $results['code'] = "10001"; 
                    $results['result'] = "Success"; 
                    $results['message'] = "User is logged In"; 
                    $results['data'] = $insuranceAgent[0];
                } else {
                    $results['code'] = $errorcode; 
                    $results['result'] = "Error"; 
                    $results['message'] = "Could not match that email and password"; 
                    $results['data'] = array();   
                    $registrationError = true;
                    $errorcode++;    
                }
            } else {
                $results['code'] = $errorcode; 
                $results['result'] = "Error"; 
                $results['message'] = "Could not match that email and password"; 
                $results['data'] = array();   
                $registrationError = true;
                $errorcode++;   
            }
            $response = $thingEngine->slim->response();
            $response['Content-Type'] = 'application/json';
            $response['X-Powered-By'] = 'Thing Engine';
            $response->status(200);
            $response->body(json_encode($results));
        })->via('GET','POST'); 
        //
        //
        //
        //
        //
        //
        $thingEngine->slim->get('/isLoggedIn', function () use ($thingEngine){
            $auth  = new \thingEngine\auth();
            $user_id = $auth->userloggedin($thingEngine->slim->getEncryptedCookie("thingCookieUserId"));
            if($user_id > 0){
                $results['code'] = "10001"; 
                $results['result'] = "Success"; 
                $results['message'] = "User is logged In"; 
                $results['data'] = $_SESSION['api']['user'];
            } else {
                $results['code'] = "10002"; 
                $results['result'] = "Error"; 
                $results['message'] = "User is not logged In"; 
                $results['data'] = array();
            }
            $response = $thingEngine->slim->response();
            $response['Content-Type'] = 'application/json';
            $response['X-Powered-By'] = 'Thing Engine';
            $response->status(200);
            $response->body(json_encode($results));
        })->via('POST', 'GET');

        //
        //
        //
        //
        //
        //
        $thingEngine->slim->get('/logout', function () use ($thingEngine){
            //$thingEngine->setEncryptedCookie("apiCookieUserId", FALSE);
            $_SESSION['api']['user'] = array();
            $results['code'] = "10001"; 
            $results['result'] = "Success"; 
            $results['message'] = "User is logged out"; 
            $results['data'] = array();
            $response = $thingEngine->slim->response();
            $response['Content-Type'] = 'application/json';
            $response['X-Powered-By'] = 'Thing Engine';
            $response->status(200);
            $response->body(json_encode($results));
        })->via('POST', 'GET');



    });
});
