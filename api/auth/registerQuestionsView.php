<?php
echo $_SESSION['test'];
?>
  <html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Page title -->
    <title>ThingCRM123</title>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->
    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>js/vendor/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>js/vendor/metisMenu/dist/metisMenu.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>js/vendor/animate.css/animate.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>js/vendor/bootstrap/dist/css/bootstrap.css">
    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>fonts/pe-icon-7-stroke/css/helper.css">
    <link rel="stylesheet" href="<?php echo $settings['baseUri'];?>styles/style.css">
    <style type="text/css">
      .jqstooltip {
        position: absolute;
        left: 0px;
        top: 0px;
        visibility: hidden;
        background: rgb(0, 0, 0) transparent;
        background-color: rgba(0, 0, 0, 0.6);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
        -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
        color: white;
        font: 10px arial, san serif;
        text-align: left;
        white-space: nowrap;
        padding: 5px;
        border: 1px solid white;
        z-index: 10000;
      }
      
      .jqsfield {
        color: white;
        font: 10px arial, san serif;
        text-align: left;
      }

    </style>


  </head>

  <body class="blank">
    <!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    <div class="color-line"></div>
    <div class="register-container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center m-b-md">
            <h3>Questions</h3>
            <small>Please Answer All Questions as Accurately as Possible.</small>
            <?php
    		
              if(!empty($message)){
						?>
              <p class="text-danger">
                <?php echo $message;?>
              </p>
              <?php
						}
	?>
          </div>
          <div class="hpanel">
            <div class="panel-body">
              <form method="post" id="registerForm">
                <div class="row">
                  <div class="form-group col-lg-6">
                    <label for="licensedStates">Licensed States</label>
                    <br>(Hold CTRL button to select multiple)
                    <select id="licensedStates" name="licensedStates[]" class="form-control" multiple="1" required="1" placeholder="State License" tabindex="10" style="min-height: 100px;">
                      <option value="NONE">I Am Not Currently Licensed</option>
                      <option value="AL">Alabama</option>
                      <option value="AK">Alaska</option>
                      <option value="AZ">Arizona</option>
                      <option value="AR">Arkansas</option>
                      <option value="CA">California</option>
                      <option value="CO">Colorado</option>
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="DC">District Of Columbia</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="HI">Hawaii</option>
                      <option value="ID">Idaho</option>
                      <option value="IL">Illinois</option>
                      <option value="IN">Indiana</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="MT">Montana</option>
                      <option value="NE">Nebraska</option>
                      <option value="NV">Nevada</option>
                      <option value="NH">New Hampshire</option>
                      <option value="NJ">New Jersey</option>
                      <option value="NM">New Mexico</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="ND">North Dakota</option>
                      <option value="OH">Ohio</option>
                      <option value="OK">Oklahoma</option>
                      <option value="OR">Oregon</option>
                      <option value="PA">Pennsylvania</option>
                      <option value="RI">Rhode Island</option>
                      <option value="SC">South Carolina</option>
                      <option value="SD">South Dakota</option>
                      <option value="TN">Tennessee</option>
                      <option value="TX">Texas</option>
                      <option value="UT">Utah</option>
                      <option value="VT">Vermont</option>
                      <option value="VA">Virginia</option>
                      <option value="WA">Washington</option>
                      <option value="WV">West Virginia</option>
                      <option value="WI">Wisconsin</option>
                      <option value="WY">Wyoming</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6">
                    <label for="licenses">Type of Licenses</label>
                    <br>(Hold CTRL button to select multiple)
                    <select id="licenses" name="licenses[]" class="form-control" multiple="1" placeholder="License Types" tabindex="11" style="min-height: 100px;">
                      <option value="Health">HEALTH</option>
                      <option value="Life">LIFE</option>
                      <option value="P&amp;C">P&amp;C</option>
                      <option value="Financial">FINANCIAL</option>
                    </select>
                  </div>

                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationViolate1994CrimeAct">Will you be in violation of the 1994 Crime Act if you act as an insurance agent?</label>
                    <select id="insuranceRegistrationViolate1994CrimeAct" name="insuranceRegistrationViolate1994CrimeAct" class="form-control" required="1" placeholder="">
                      <option value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="insuranceRegistrationViolate1994CrimeActdiv" style="display:none">
                    <label for="insuranceRegistrationViolate1994CrimeActExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationViolate1994CrimeActExplain" class="form-control" name="insuranceRegistrationViolate1994CrimeActExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationFile1033">Did you file a 1033 form in any state due to felony charges covered by 18USC 1033?</label>
                    <select id="" name="insuranceRegistrationFile1033" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationFile1033Explain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationFile1033Explain" class="form-control" name="insuranceRegistrationFile1033Explain"> </div>

                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationFiledBankruptcy">Have you ever filed Bankruptcy?</label>
                    <select id="insuranceRegistrationFiledBankruptcy" name="insuranceRegistrationFiledBankruptcy" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationFiledBankruptcyExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationFiledBankruptcyExplain" class="form-control" name="insuranceRegistrationFiledBankruptcyExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationConvictedOfCrime">Are you currently charged with or have you ever been convicted of a crime, including felony, misdemeanor, or military offense?</label>
                    <select id="insuranceRegistrationConvictedOfCrime" name="insuranceRegistrationConvictedOfCrime" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationConvictedOfCrimeExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationConvictedOfCrimeExplain" class="form-control" name="insuranceRegistrationConvictedOfCrimeExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationOutstandingDebts">Do you have any outstanding debts with any insurance companies?</label>
                    <select id="insuranceRegistrationOutstandingDebts" name="insuranceRegistrationOutstandingDebts" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationOutstandingDebtsExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationOutstandingDebtsExplain" class="form-control" name="insuranceRegistrationOutstandingDebtsExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationTaxLien">Do you currently have a state, federal or any taxing authority tax lien?</label>
                    <select id="insuranceRegistrationTaxLien" name="insuranceRegistrationTaxLien" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationTaxLienExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationTaxLienExplain" class="form-control" name="insuranceRegistrationTaxLienExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationCivilJudgements">Do you have any outstanding civil judgments?</label>
                    <select id="insuranceRegistrationCivilJudgements" name="insuranceRegistrationCivilJudgements" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationCivilJudgementsExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationCivilJudgementsExplain" class="form-control" name="insuranceRegistrationCivilJudgementsExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationRefusedBond">Have you ever been refused a bond or had a bond cancelled?</label>
                    <select id="insuranceRegistrationRefusedBond" name="insuranceRegistrationRefusedBond" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationRefusedBondExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationRefusedBondExplain" class="form-control" name="insuranceRegistrationRefusedBondExplain">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationFinraSanctions">Have you ever been named or involved as a party in an administrative proceeding including but not limited to FINRA sanctions or arbitration proceeding regarding any professional or occupational license or registration?</label>
                    <select id="insuranceRegistrationFinraSanctions" name="insuranceRegistrationFinraSanctions" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationFinraSanctionsExplain">If Yes, Explain and Includes State Insurance Department investigations, license suspensions, Revocations, or administrative fines:</label>
                    <input type="text" id="insuranceRegistrationFinraSanctionsExplain" class="form-control" name="insuranceRegistrationFinraSanctionsExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationInsuranceLitigation">Are you now or have you ever been included in litigation with an insurance company that you represented?</label>
                    <select id="insuranceRegistrationInsuranceLitigation" name="insuranceRegistrationInsuranceLitigation" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationInsuranceLitigationExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationInsuranceLitigationExplain" class="form-control" name="insuranceRegistrationInsuranceLitigationExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationClaimProfessionalLiability">Have you ever had a claim filed against your professional liability or errors and omissions insurance coverage?</label>
                    <select id="insuranceRegistrationClaimProfessionalLiability" name="insuranceRegistrationClaimProfessionalLiability" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationClaimProfessionalLiabilityExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationClaimProfessionalLiabilityExplain" class="form-control" name="Explain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationSubjectComplaint">Have you ever been the subject of a consumer-initiated complaint, proceeding or investigation by any self-regulatory body, securities commodities, insurance regulatory body/organization, employer or insurer?</label>
                    <select id="insuranceRegistrationSubjectComplaint" name="insuranceRegistrationSubjectComplaint" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationSubjectComplaintExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationSubjectComplaintExplain" class="form-control" name="insuranceRegistrationSubjectComplaintExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationResignReason">Has any insurance or financial services employer, broker-dealer, or insurer terminated your contract or permitted you to resign for reason other than lack of sales?</label>
                    <select id="insuranceRegistrationResignReason" name="insuranceRegistrationResignReason" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationResignReasonExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationResignReasonExplain" class="form-control" name="insuranceRegistrationResignReasonExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationDisciplineFines">Has any insurance department, government agency, securities, commodities. Or self-regulatory authority ever denied, suspend, revoked, censured, barred, or otherwise disciplined your membership, license, registration, or disciplined
                      you with fines or by restricting your activities?</label>
                    <select id="insuranceRegistrationDisciplineFines" name="insuranceRegistrationDisciplineFines" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationDisciplineFinesExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationDisciplineFinesExplain" class="form-control" name="insuranceRegistrationDisciplineFinesExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationLawsuit">Are you currently a party to, or have you ever been found liable in, any lawsuit or arbitration proceeding involving allegations of fraud, misappropriation or conversions of funds, misrepresentations or breach of fiduciary duty?</label>
                    <select id="insuranceRegistrationLawsuit" name="insuranceRegistrationLawsuit" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationLawsuitExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationLawsuitExplain" class="form-control" name="insuranceRegistrationLawsuitExplain"> </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="insuranceRegistrationConvictedViolence">Have you ever been convicted of, plead no contest to, or are you currently charged with committing a crime whether or not adjudication was withheld, related to felony theft, fraud, mishandling funds, breach of trust, or dishonesty,
                      or a felony related to violence including but limited to Assault, Battery or Kidnapping?</label>
                    <select id="insuranceRegistrationConvictedViolence" name="insuranceRegistrationConvictedViolence" class="form-control" required="1" placeholder="">
                      <option selected="selected" value=""></option>
                      <option value="yes">Yes</option>
                      <option value="no">No</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-6" id="">
                    <label for="insuranceRegistrationConvictedViolenceExplain">If Yes, Explain:</label>
                    <input type="text" id="insuranceRegistrationConvictedViolenceExplain" class="form-control" name="insuranceRegistrationConvictedViolenceExplain"> </div>
                </div>

                <div class="row">
                  <div class="form-group col-lg-12 text-center">
                    <label for="agreeToTerms">Agree to Terms of Service</label>
                    <br>
                    <div class="icheckbox_square-green" style="position: relative;">
                      <input type="checkbox" class="i-checks" style="position: absolute; opacity: 0;" id="agreeToTerms" name="agreeToTerms">
                    </div>
                    Yes, I agree to the Terms of Service.

                  </div>
                </div>
                <div class="row">
                  <div class="text-center">
                    <button type="submit" class="btn btn-success">Register</button>
                    <a href="<?php echo $settings['baseUri'];?>api/auth/login" class="btn btn-default">Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          <br> 2016 Copyright Insurance CRM
        </div>
      </div>
    </div>
    <!-- Vendor scripts -->
    <script src="<?php echo $settings['baseUri'];?>js/vendor/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo $settings['baseUri'];?>js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo $settings['baseUri'];?>js/vendor/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $settings['baseUri'];?>js/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo $settings['baseUri'];?>js/vendor/metisMenu/dist/metisMenu.min.js"></script>
    <script src="<?php echo $settings['baseUri'];?>js/vendor/iCheck/icheck.min.js"></script>
    <script src="<?php echo $settings['baseUri'];?>js/vendor/sparkline/index.js"></script>
    <script src="<?php echo $settings['baseUri'];?>js/vendor/jquery-validation/jquery.validate.min.js"></script>
    <!-- App scripts -->
    <script src="<?php echo $settings['baseUri'];?>js/homer.js"></script>

    <script type="text/javascript">
       $('#insuranceRegistrationViolate1994CrimeAct').change(function() {
   if ($(this).val() == 'yes') {
     $('#insuranceRegistrationViolate1994CrimeActdiv').show();
   } else {
     $('#insuranceRegistrationViolate1994CrimeActdiv').hide();
   }
 });
      });
      $(function() {
        $('#registerForm').validate({
          rules: {

            licensedStates: {
              required: true,
            },



          },
          messages: {
            passwordConf: {
              equalTo: 'Passwords Must Match'
            },
          },
          submitHandler: function(form) {
            $.post('<?php echo $settings['
              baseUri '];?>api/auth/registerSave', $('#registerForm').serialize(),
              function(response) {
                window.location = '<?php echo $settings['
                baseUri '];?>api/auth/login';
              });

          },
          errorPlacement: function(error, element) {
            $(element)
              .closest("form")
              .find("label[for='" + element.attr("id") + "']")
              .append(error);
          },
          errorElement: "span",
        });
      });

    </script>
  </body>

  </html>
