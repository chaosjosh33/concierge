<?php
/*
$auth  = new \thingEngine\auth();
    $user_id = $auth->userloggedin($thingEngine->slim->getEncryptedCookie("thingCookieUserId"));
    if($user_id < 0){
        header('location: '.$settings['base_uri'].$settings['api_uri'].'/auth/login');
    }
*/
/*
MIDDLEWARE
*/
$authLoggedIn = function () use ($thingEngine ) {
    $auth  = new \thingEngine\auth();
    $user_id = $auth->userloggedin($thingEngine->slim->getEncryptedCookie("thingCookieUserId"));
    //if($user_id < 1){
    //     $thingEngine->slim->flash('error', 'Login required');
    //     $thingEngine->slim->redirect($thingEngine->settings['base_uri'].'api/auth/login');
    //    return false; 
    //}
    /*
        $user = new auth($thingEngine->settings);
        if ( $user->user_logged_in() === false ) {
            $thingEngine->slim->flash('error', 'Login required');
            $thingEngine->slim->redirect($thingEngine->settings['base_uri']);
            return false;
        }
        */
};
$authPermission = function ( $role = 'member') use ( $thingEngine ) {
    return function () use ( $thingEngine, $role ) {
        $user = new auth($thingEngine->settings);
        if ( $user->user_logged_in() === false ) {
            $thingEngine->slim->flash('error', 'Login required');
            $thingEngine->slim->redirect($thingEngine->settings['base_uri']);
            return false;
        }
    };
};
/*
Debug Output - Just a way to output an array for debugging
*/
function debug($arr, $label = "Debug Array"){
    echo "<PRE><hr>";
    echo "<h2>".$label."</h2>";
    print_r($arr);
    echo "<hr></pre>";
}
function peakmemory()
{
    echo " Peak Memory: ".convertmemory(memory_get_peak_usage(true));
}
function convertmemory($size) {
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}
