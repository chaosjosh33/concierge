<?php
namespace thingEngine;
// PERSON EXTENSION
class person extends \thingEngine\thingExtension{
    protected $personTable;
    protected $firstName;
    protected $middleName;
    protected $lastName;
    protected $firstNameAlt;
    protected $middleNameAlt;
    protected $lastNameAlt;
    protected $prefixName;
    protected $suffixName;
    protected $prefixNameAlt;
    protected $suffixNameAlt;
    protected $dateOfBirth;
    protected $age;
    protected $birthplace;
    protected $dateOfDeath;
    protected $deathPlace;
    protected $citizenOf;
    protected $gender;
    protected $weightLbs;
    protected $heightFeet;
    protected $heightInches;
    protected $driversLicense;
    protected $driversLicenseState;
    protected $socialSecurityNumber;
    protected $phoneDaytime;
    protected $phoneEvening;
    protected $phoneWork;
    protected $phoneWorkExtension;
    protected $phoneCell;
    protected $emailPrimary;
    protected $emailSecondary;
    protected $preferredSpokenLanguage;
    protected $preferredWrittenLanguage;
    protected $smoker;
    protected $drinker;
    protected $drugUse;

    //Construct
    function __construct($id = false){
        $this->personTable = "persons";
        $this->extensionInfo['name'] = "Person Extension";
        $this->extensionInfo['description'] = "Basic Person Structure";
        $this->extensionInfo['author'] = "ThingEngine";
        $this->extensionInfo['owner'] = "thingengine";
        $this->extensionInfo['price'] = "0.00";
        $this->extensionInfo['version'] = "1.0";
        $this->extensionInfo['dateCreated'] = "12/01/2015";
    }
    // LOAD PERSON
    public function loadPerson($id){
        $person = \RedBeanPHP\Facade::load( $this->personTable , $id);
        if(!empty($person)){
            foreach ($person as $key=>$value){
                $this->$key = $value;
            }
        }
    }
    // SAVE PERSON 
    public function savePerson(){
        $this->saveData($this->personTable);
    }
    public function setPersonTable($tableName){
        $this->personTable = $tableName;
    }
 

}