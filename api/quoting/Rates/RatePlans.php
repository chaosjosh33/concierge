<?php

class RatePlans
{

    /**
     * @var RatePlansRequest $request
     */
    protected $request = null;

    /**
     * @param RatePlansRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RatePlansRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RatePlansRequest $request
     * @return RatePlans
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
