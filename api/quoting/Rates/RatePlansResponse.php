<?php

class RatePlansResponse
{

    /**
     * @var RatePlansResponse $RatePlansResult
     */
    protected $RatePlansResult = null;

    /**
     * @param RatePlansResponse $RatePlansResult
     */
    public function __construct($RatePlansResult)
    {
      $this->RatePlansResult = $RatePlansResult;
    }

    /**
     * @return RatePlansResponse
     */
    public function getRatePlansResult()
    {
      return $this->RatePlansResult;
    }

    /**
     * @param RatePlansResponse $RatePlansResult
     * @return RatePlansResponse
     */
    public function setRatePlansResult($RatePlansResult)
    {
      $this->RatePlansResult = $RatePlansResult;
      return $this;
    }

}
