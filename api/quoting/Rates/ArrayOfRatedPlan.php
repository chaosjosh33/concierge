<?php

class ArrayOfRatedPlan implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RatedPlan[] $RatedPlan
     */
    protected $RatedPlan = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RatedPlan[]
     */
    public function getRatedPlan()
    {
      return $this->RatedPlan;
    }

    /**
     * @param RatedPlan[] $RatedPlan
     * @return ArrayOfRatedPlan
     */
    public function setRatedPlan(array $RatedPlan = null)
    {
      $this->RatedPlan = $RatedPlan;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RatedPlan[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RatedPlan
     */
    public function offsetGet($offset)
    {
      return $this->RatedPlan[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RatedPlan $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->RatedPlan[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RatedPlan[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RatedPlan Return the current element
     */
    public function current()
    {
      return current($this->RatedPlan);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RatedPlan);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RatedPlan);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RatedPlan);
    }

    /**
     * Countable implementation
     *
     * @return RatedPlan Return count of elements
     */
    public function count()
    {
      return count($this->RatedPlan);
    }

}
