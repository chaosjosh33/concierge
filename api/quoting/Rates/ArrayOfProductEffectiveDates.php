<?php

class ArrayOfProductEffectiveDates implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductEffectiveDates[] $ProductEffectiveDates
     */
    protected $ProductEffectiveDates = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductEffectiveDates[]
     */
    public function getProductEffectiveDates()
    {
      return $this->ProductEffectiveDates;
    }

    /**
     * @param ProductEffectiveDates[] $ProductEffectiveDates
     * @return ArrayOfProductEffectiveDates
     */
    public function setProductEffectiveDates(array $ProductEffectiveDates = null)
    {
      $this->ProductEffectiveDates = $ProductEffectiveDates;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductEffectiveDates[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductEffectiveDates
     */
    public function offsetGet($offset)
    {
      return $this->ProductEffectiveDates[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductEffectiveDates $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ProductEffectiveDates[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductEffectiveDates[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductEffectiveDates Return the current element
     */
    public function current()
    {
      return current($this->ProductEffectiveDates);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductEffectiveDates);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductEffectiveDates);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductEffectiveDates);
    }

    /**
     * Countable implementation
     *
     * @return ProductEffectiveDates Return count of elements
     */
    public function count()
    {
      return count($this->ProductEffectiveDates);
    }

}
