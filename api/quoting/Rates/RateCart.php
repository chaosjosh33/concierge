<?php

class RateCart
{

    /**
     * @var RateCartRequest $request
     */
    protected $request = null;

    /**
     * @param RateCartRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RateCartRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RateCartRequest $request
     * @return RateCart
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
