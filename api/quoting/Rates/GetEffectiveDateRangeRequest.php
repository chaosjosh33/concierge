<?php

class GetEffectiveDateRangeRequest
{

    /**
     * @var string $BrokerId
     */
    protected $BrokerId = null;

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBrokerId()
    {
      return $this->BrokerId;
    }

    /**
     * @param string $BrokerId
     * @return GetEffectiveDateRangeRequest
     */
    public function setBrokerId($BrokerId)
    {
      $this->BrokerId = $BrokerId;
      return $this;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return GetEffectiveDateRangeRequest
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

}
