<?php

class BenefitType
{
    const __default = 'Unknown';
    const Unknown = 'Unknown';
    const Deductible = 'Deductible';
    const Coinsurance = 'Coinsurance';
    const CoinsuranceMaximumOOP = 'CoinsuranceMaximumOOP';
    const CoveragePeriod = 'CoveragePeriod';
    const CoveragePeriodMaximum = 'CoveragePeriodMaximum';
    const EnrollmentFee = 'EnrollmentFee';
    const AssociationFee = 'AssociationFee';
    const AdminFee = 'AdminFee';
    const NetworkFee = 'NetworkFee';
    const LifeTimeMaximum = 'LifeTimeMaximum';
    const TransplantBenefit = 'TransplantBenefit';
    const OfficeVisitBenefit = 'OfficeVisitBenefit';
    const DentalLevel = 'DentalLevel';
    const VisionRider = 'VisionRider';
    const ProductFees = 'ProductFees';
    const AMEFaceValue = 'AMEFaceValue';


}
