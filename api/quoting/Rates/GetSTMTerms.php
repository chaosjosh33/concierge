<?php

class GetSTMTerms
{

    /**
     * @var GetTermsRequest $request
     */
    protected $request = null;

    /**
     * @param GetTermsRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return GetTermsRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GetTermsRequest $request
     * @return GetSTMTerms
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
