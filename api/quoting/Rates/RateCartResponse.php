<?php

class RateCartResponse
{

    /**
     * @var RateCartResponse $RateCartResult
     */
    protected $RateCartResult = null;

    /**
     * @param RateCartResponse $RateCartResult
     */
    public function __construct($RateCartResult)
    {
      $this->RateCartResult = $RateCartResult;
    }

    /**
     * @return RateCartResponse
     */
    public function getRateCartResult()
    {
      return $this->RateCartResult;
    }

    /**
     * @param RateCartResponse $RateCartResult
     * @return RateCartResponse
     */
    public function setRateCartResult($RateCartResult)
    {
      $this->RateCartResult = $RateCartResult;
      return $this;
    }

}
