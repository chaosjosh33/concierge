<?php

class ProductTypeId
{
    const __default = 'Unknown';
    const Unknown = 'Unknown';
    const STM = 'STM';
    const NHICSupplemental = 'NHICSupplemental';
    const TIC = 'TIC';
    const StarmountDental = 'StarmountDental';
    const Association = 'Association';


}
