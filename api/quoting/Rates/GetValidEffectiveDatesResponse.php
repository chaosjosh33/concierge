<?php

class GetValidEffectiveDatesResponse
{

    /**
     * @var GetValidEffectiveDatesResponse $GetValidEffectiveDatesResult
     */
    protected $GetValidEffectiveDatesResult = null;

    /**
     * @param GetValidEffectiveDatesResponse $GetValidEffectiveDatesResult
     */
    public function __construct($GetValidEffectiveDatesResult)
    {
      $this->GetValidEffectiveDatesResult = $GetValidEffectiveDatesResult;
    }

    /**
     * @return GetValidEffectiveDatesResponse
     */
    public function getGetValidEffectiveDatesResult()
    {
      return $this->GetValidEffectiveDatesResult;
    }

    /**
     * @param GetValidEffectiveDatesResponse $GetValidEffectiveDatesResult
     * @return GetValidEffectiveDatesResponse
     */
    public function setGetValidEffectiveDatesResult($GetValidEffectiveDatesResult)
    {
      $this->GetValidEffectiveDatesResult = $GetValidEffectiveDatesResult;
      return $this;
    }

}
