<?php

class GetState
{

    /**
     * @var GetStateRequest $request
     */
    protected $request = null;

    /**
     * @param GetStateRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return GetStateRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GetStateRequest $request
     * @return GetState
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
