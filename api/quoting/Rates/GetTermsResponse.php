<?php

class GetTermsResponse
{

    /**
     * @var ArrayOfTerm $Terms
     */
    protected $Terms = null;

    /**
     * @var ErrorInformation $ValidationErrorInformation
     */
    protected $ValidationErrorInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfTerm
     */
    public function getTerms()
    {
      return $this->Terms;
    }

    /**
     * @param ArrayOfTerm $Terms
     * @return GetTermsResponse
     */
    public function setTerms($Terms)
    {
      $this->Terms = $Terms;
      return $this;
    }

    /**
     * @return ErrorInformation
     */
    public function getValidationErrorInformation()
    {
      return $this->ValidationErrorInformation;
    }

    /**
     * @param ErrorInformation $ValidationErrorInformation
     * @return GetTermsResponse
     */
    public function setValidationErrorInformation($ValidationErrorInformation)
    {
      $this->ValidationErrorInformation = $ValidationErrorInformation;
      return $this;
    }

}
