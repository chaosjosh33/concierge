<?php

class GetValidEffectiveDatesRequest
{

    /**
     * @var string $BrokerId
     */
    protected $BrokerId = null;

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var int $ProductId
     */
    protected $ProductId = null;

    /**
     * @var \DateTime $RequestedEffectiveDate
     */
    protected $RequestedEffectiveDate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBrokerId()
    {
      return $this->BrokerId;
    }

    /**
     * @param string $BrokerId
     * @return GetValidEffectiveDatesRequest
     */
    public function setBrokerId($BrokerId)
    {
      $this->BrokerId = $BrokerId;
      return $this;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return GetValidEffectiveDatesRequest
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
      return $this->ProductId;
    }

    /**
     * @param int $ProductId
     * @return GetValidEffectiveDatesRequest
     */
    public function setProductId($ProductId)
    {
      $this->ProductId = $ProductId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRequestedEffectiveDate()
    {
      if ($this->RequestedEffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->RequestedEffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $RequestedEffectiveDate
     * @return GetValidEffectiveDatesRequest
     */
    public function setRequestedEffectiveDate(\DateTime $RequestedEffectiveDate = null)
    {
      if ($RequestedEffectiveDate == null) {
       $this->RequestedEffectiveDate = null;
      } else {
        $this->RequestedEffectiveDate = $RequestedEffectiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

}
