<?php

class ArrayOfTerm implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Term[] $Term
     */
    protected $Term = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Term[]
     */
    public function getTerm()
    {
      return $this->Term;
    }

    /**
     * @param Term[] $Term
     * @return ArrayOfTerm
     */
    public function setTerm(array $Term = null)
    {
      $this->Term = $Term;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Term[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Term
     */
    public function offsetGet($offset)
    {
      return $this->Term[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Term $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Term[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Term[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Term Return the current element
     */
    public function current()
    {
      return current($this->Term);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Term);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Term);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Term);
    }

    /**
     * Countable implementation
     *
     * @return Term Return count of elements
     */
    public function count()
    {
      return count($this->Term);
    }

}
