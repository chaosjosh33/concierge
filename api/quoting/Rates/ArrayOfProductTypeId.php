<?php

class ArrayOfProductTypeId implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductTypeId[] $ProductTypeId
     */
    protected $ProductTypeId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductTypeId[]
     */
    public function getProductTypeId()
    {
      return $this->ProductTypeId;
    }

    /**
     * @param ProductTypeId[] $ProductTypeId
     * @return ArrayOfProductTypeId
     */
    public function setProductTypeId(array $ProductTypeId = null)
    {
      $this->ProductTypeId = $ProductTypeId;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductTypeId[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductTypeId
     */
    public function offsetGet($offset)
    {
      return $this->ProductTypeId[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductTypeId $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ProductTypeId[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductTypeId[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductTypeId Return the current element
     */
    public function current()
    {
      return current($this->ProductTypeId);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductTypeId);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductTypeId);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductTypeId);
    }

    /**
     * Countable implementation
     *
     * @return ProductTypeId Return count of elements
     */
    public function count()
    {
      return count($this->ProductTypeId);
    }

}
