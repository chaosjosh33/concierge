<?php

class PlanTypeId
{
    const __default = 'Unknown';
    const Unknown = 'Unknown';
    const ShortTermMedical = 'ShortTermMedical';
    const AcciMed = 'AcciMed';
    const TrioMed = 'TrioMed';
    const TICDental = 'TICDental';
    const TICCHS = 'TICCHS';
    const TICAME = 'TICAME';
    const TICCI = 'TICCI';
    const TICAccident = 'TICAccident';
    const StarmountDental = 'StarmountDental';
    const LifeAssociation = 'LifeAssociation';


}
