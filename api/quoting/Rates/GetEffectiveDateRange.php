<?php

class GetEffectiveDateRange
{

    /**
     * @var GetEffectiveDateRangeRequest $request
     */
    protected $request = null;

    /**
     * @param GetEffectiveDateRangeRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return GetEffectiveDateRangeRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GetEffectiveDateRangeRequest $request
     * @return GetEffectiveDateRange
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
