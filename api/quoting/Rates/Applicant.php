<?php

class Applicant
{

    /**
     * @var \DateTime $DOB
     */
    protected $DOB = null;

    /**
     * @var int $Gender
     */
    protected $Gender = null;

    /**
     * @var int $RelationshipType
     */
    protected $RelationshipType = null;

    /**
     * @var boolean $Smoker
     */
    protected $Smoker = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getDOB()
    {
      if ($this->DOB == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DOB);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DOB
     * @return Applicant
     */
    public function setDOB(\DateTime $DOB = null)
    {
      if ($DOB == null) {
       $this->DOB = null;
      } else {
        $this->DOB = $DOB->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getGender()
    {
      return $this->Gender;
    }

    /**
     * @param int $Gender
     * @return Applicant
     */
    public function setGender($Gender)
    {
      $this->Gender = $Gender;
      return $this;
    }

    /**
     * @return int
     */
    public function getRelationshipType()
    {
      return $this->RelationshipType;
    }

    /**
     * @param int $RelationshipType
     * @return Applicant
     */
    public function setRelationshipType($RelationshipType)
    {
      $this->RelationshipType = $RelationshipType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSmoker()
    {
      return $this->Smoker;
    }

    /**
     * @param boolean $Smoker
     * @return Applicant
     */
    public function setSmoker($Smoker)
    {
      $this->Smoker = $Smoker;
      return $this;
    }

}
