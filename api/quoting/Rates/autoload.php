<?php


 function autoload_e2172fad4bba6fd276342828b02f8551($class)
{
    $classes = array(
        'QuotingBroker' => __DIR__ .'/QuotingBroker.php',
        'ArrayOfdateTime' => __DIR__ .'/ArrayOfdateTime.php',
        'ArrayOfstring' => __DIR__ .'/ArrayOfstring.php',
        'GetValidEffectiveDatesRequest' => __DIR__ .'/GetValidEffectiveDatesRequest.php',
        'Credentials' => __DIR__ .'/Credentials.php',
        'GetValidEffectiveDatesResponse' => __DIR__ .'/GetValidEffectiveDatesResponse.php',
        'ArrayOfProductEffectiveDates' => __DIR__ .'/ArrayOfProductEffectiveDates.php',
        'ProductEffectiveDates' => __DIR__ .'/ProductEffectiveDates.php',
        'ErrorInformation' => __DIR__ .'/ErrorInformation.php',
        'GetEffectiveDateRangeRequest' => __DIR__ .'/GetEffectiveDateRangeRequest.php',
        'GetEffectiveDateRangeResponse' => __DIR__ .'/GetEffectiveDateRangeResponse.php',
        'GetStateRequest' => __DIR__ .'/GetStateRequest.php',
        'GetStateResponse' => __DIR__ .'/GetStateResponse.php',
        'RatePlansRequest' => __DIR__ .'/RatePlansRequest.php',
        'ArrayOfApplicant' => __DIR__ .'/ArrayOfApplicant.php',
        'Applicant' => __DIR__ .'/Applicant.php',
        'ArrayOfProductTypeId' => __DIR__ .'/ArrayOfProductTypeId.php',
        'ProductTypeId' => __DIR__ .'/ProductTypeId.php',
        'RatePlansResponse' => __DIR__ .'/RatePlansResponse.php',
        'ArrayOfRatedPlan' => __DIR__ .'/ArrayOfRatedPlan.php',
        'RatedPlan' => __DIR__ .'/RatedPlan.php',
        'ArrayOfBenefit' => __DIR__ .'/ArrayOfBenefit.php',
        'Benefit' => __DIR__ .'/Benefit.php',
        'BenefitType' => __DIR__ .'/BenefitType.php',
        'ArrayOfMemberRate' => __DIR__ .'/ArrayOfMemberRate.php',
        'MemberRate' => __DIR__ .'/MemberRate.php',
        'PlanTypeId' => __DIR__ .'/PlanTypeId.php',
        'ArrayOfRider' => __DIR__ .'/ArrayOfRider.php',
        'Rider' => __DIR__ .'/Rider.php',
        'GetTermsRequest' => __DIR__ .'/GetTermsRequest.php',
        'GetTermsResponse' => __DIR__ .'/GetTermsResponse.php',
        'ArrayOfTerm' => __DIR__ .'/ArrayOfTerm.php',
        'Term' => __DIR__ .'/Term.php',
        'RateCartRequest' => __DIR__ .'/RateCartRequest.php',
        'RateCartResponse' => __DIR__ .'/RateCartResponse.php',
        'GetValidEffectiveDates' => __DIR__ .'/GetValidEffectiveDates.php',
        'GetEffectiveDateRange' => __DIR__ .'/GetEffectiveDateRange.php',
        'GetState' => __DIR__ .'/GetState.php',
        'RatePlans' => __DIR__ .'/RatePlans.php',
        'GetSTMTerms' => __DIR__ .'/GetSTMTerms.php',
        'GetSTMTermsResponse' => __DIR__ .'/GetSTMTermsResponse.php',
        'RateCart' => __DIR__ .'/RateCart.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_e2172fad4bba6fd276342828b02f8551');

// Do nothing. The rest is just leftovers from the code generation.
{
}
