<?php

class ArrayOfBenefit implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Benefit[] $Benefit
     */
    protected $Benefit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Benefit[]
     */
    public function getBenefit()
    {
      return $this->Benefit;
    }

    /**
     * @param Benefit[] $Benefit
     * @return ArrayOfBenefit
     */
    public function setBenefit(array $Benefit = null)
    {
      $this->Benefit = $Benefit;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Benefit[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Benefit
     */
    public function offsetGet($offset)
    {
      return $this->Benefit[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Benefit $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Benefit[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Benefit[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Benefit Return the current element
     */
    public function current()
    {
      return current($this->Benefit);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Benefit);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Benefit);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Benefit);
    }

    /**
     * Countable implementation
     *
     * @return Benefit Return count of elements
     */
    public function count()
    {
      return count($this->Benefit);
    }

}
