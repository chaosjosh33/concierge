<?php

class ArrayOfRider implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Rider[] $Rider
     */
    protected $Rider = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Rider[]
     */
    public function getRider()
    {
      return $this->Rider;
    }

    /**
     * @param Rider[] $Rider
     * @return ArrayOfRider
     */
    public function setRider(array $Rider = null)
    {
      $this->Rider = $Rider;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Rider[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Rider
     */
    public function offsetGet($offset)
    {
      return $this->Rider[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Rider $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Rider[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Rider[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Rider Return the current element
     */
    public function current()
    {
      return current($this->Rider);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Rider);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Rider);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Rider);
    }

    /**
     * Countable implementation
     *
     * @return Rider Return count of elements
     */
    public function count()
    {
      return count($this->Rider);
    }

}
