<?php

class RatedPlan
{

    /**
     * @var string $ApplicationType
     */
    protected $ApplicationType = null;

    /**
     * @var ArrayOfstring $BaseProducts
     */
    protected $BaseProducts = null;

    /**
     * @var ArrayOfBenefit $Benefits
     */
    protected $Benefits = null;

    /**
     * @var \DateTime $EffectiveDate
     */
    protected $EffectiveDate = null;

    /**
     * @var string $EnrollerId
     */
    protected $EnrollerId = null;

    /**
     * @var float $InsuranceRate
     */
    protected $InsuranceRate = null;

    /**
     * @var boolean $IsAMERider
     */
    protected $IsAMERider = null;

    /**
     * @var int $IssueType
     */
    protected $IssueType = null;

    /**
     * @var ArrayOfMemberRate $MemberRates
     */
    protected $MemberRates = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $PathToBrochure
     */
    protected $PathToBrochure = null;

    /**
     * @var string $PlanKey
     */
    protected $PlanKey = null;

    /**
     * @var PlanTypeId $PlanType
     */
    protected $PlanType = null;

    /**
     * @var string $ProductCode
     */
    protected $ProductCode = null;

    /**
     * @var string $ProductSubCode
     */
    protected $ProductSubCode = null;

    /**
     * @var ProductTypeId $ProductType
     */
    protected $ProductType = null;

    /**
     * @var float $Rate
     */
    protected $Rate = null;

    /**
     * @var ArrayOfRider $Riders
     */
    protected $Riders = null;

    /**
     * @var float $TotalRate
     */
    protected $TotalRate = null;

    /**
     * @var string $WhomCovered
     */
    protected $WhomCovered = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getApplicationType()
    {
      return $this->ApplicationType;
    }

    /**
     * @param string $ApplicationType
     * @return RatedPlan
     */
    public function setApplicationType($ApplicationType)
    {
      $this->ApplicationType = $ApplicationType;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getBaseProducts()
    {
      return $this->BaseProducts;
    }

    /**
     * @param ArrayOfstring $BaseProducts
     * @return RatedPlan
     */
    public function setBaseProducts($BaseProducts)
    {
      $this->BaseProducts = $BaseProducts;
      return $this;
    }

    /**
     * @return ArrayOfBenefit
     */
    public function getBenefits()
    {
      return $this->Benefits;
    }

    /**
     * @param ArrayOfBenefit $Benefits
     * @return RatedPlan
     */
    public function setBenefits($Benefits)
    {
      $this->Benefits = $Benefits;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
      if ($this->EffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EffectiveDate
     * @return RatedPlan
     */
    public function setEffectiveDate(\DateTime $EffectiveDate = null)
    {
      if ($EffectiveDate == null) {
       $this->EffectiveDate = null;
      } else {
        $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getEnrollerId()
    {
      return $this->EnrollerId;
    }

    /**
     * @param string $EnrollerId
     * @return RatedPlan
     */
    public function setEnrollerId($EnrollerId)
    {
      $this->EnrollerId = $EnrollerId;
      return $this;
    }

    /**
     * @return float
     */
    public function getInsuranceRate()
    {
      return $this->InsuranceRate;
    }

    /**
     * @param float $InsuranceRate
     * @return RatedPlan
     */
    public function setInsuranceRate($InsuranceRate)
    {
      $this->InsuranceRate = $InsuranceRate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsAMERider()
    {
      return $this->IsAMERider;
    }

    /**
     * @param boolean $IsAMERider
     * @return RatedPlan
     */
    public function setIsAMERider($IsAMERider)
    {
      $this->IsAMERider = $IsAMERider;
      return $this;
    }

    /**
     * @return int
     */
    public function getIssueType()
    {
      return $this->IssueType;
    }

    /**
     * @param int $IssueType
     * @return RatedPlan
     */
    public function setIssueType($IssueType)
    {
      $this->IssueType = $IssueType;
      return $this;
    }

    /**
     * @return ArrayOfMemberRate
     */
    public function getMemberRates()
    {
      return $this->MemberRates;
    }

    /**
     * @param ArrayOfMemberRate $MemberRates
     * @return RatedPlan
     */
    public function setMemberRates($MemberRates)
    {
      $this->MemberRates = $MemberRates;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return RatedPlan
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getPathToBrochure()
    {
      return $this->PathToBrochure;
    }

    /**
     * @param string $PathToBrochure
     * @return RatedPlan
     */
    public function setPathToBrochure($PathToBrochure)
    {
      $this->PathToBrochure = $PathToBrochure;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlanKey()
    {
      return $this->PlanKey;
    }

    /**
     * @param string $PlanKey
     * @return RatedPlan
     */
    public function setPlanKey($PlanKey)
    {
      $this->PlanKey = $PlanKey;
      return $this;
    }

    /**
     * @return PlanTypeId
     */
    public function getPlanType()
    {
      return $this->PlanType;
    }

    /**
     * @param PlanTypeId $PlanType
     * @return RatedPlan
     */
    public function setPlanType($PlanType)
    {
      $this->PlanType = $PlanType;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductCode()
    {
      return $this->ProductCode;
    }

    /**
     * @param string $ProductCode
     * @return RatedPlan
     */
    public function setProductCode($ProductCode)
    {
      $this->ProductCode = $ProductCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductSubCode()
    {
      return $this->ProductSubCode;
    }

    /**
     * @param string $ProductSubCode
     * @return RatedPlan
     */
    public function setProductSubCode($ProductSubCode)
    {
      $this->ProductSubCode = $ProductSubCode;
      return $this;
    }

    /**
     * @return ProductTypeId
     */
    public function getProductType()
    {
      return $this->ProductType;
    }

    /**
     * @param ProductTypeId $ProductType
     * @return RatedPlan
     */
    public function setProductType($ProductType)
    {
      $this->ProductType = $ProductType;
      return $this;
    }

    /**
     * @return float
     */
    public function getRate()
    {
      return $this->Rate;
    }

    /**
     * @param float $Rate
     * @return RatedPlan
     */
    public function setRate($Rate)
    {
      $this->Rate = $Rate;
      return $this;
    }

    /**
     * @return ArrayOfRider
     */
    public function getRiders()
    {
      return $this->Riders;
    }

    /**
     * @param ArrayOfRider $Riders
     * @return RatedPlan
     */
    public function setRiders($Riders)
    {
      $this->Riders = $Riders;
      return $this;
    }

    /**
     * @return float
     */
    public function getTotalRate()
    {
      return $this->TotalRate;
    }

    /**
     * @param float $TotalRate
     * @return RatedPlan
     */
    public function setTotalRate($TotalRate)
    {
      $this->TotalRate = $TotalRate;
      return $this;
    }

    /**
     * @return string
     */
    public function getWhomCovered()
    {
      return $this->WhomCovered;
    }

    /**
     * @param string $WhomCovered
     * @return RatedPlan
     */
    public function setWhomCovered($WhomCovered)
    {
      $this->WhomCovered = $WhomCovered;
      return $this;
    }

}
