<?php

class QuotingBroker extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'ArrayOfdateTime' => '\\ArrayOfdateTime',
      'ArrayOfstring' => '\\ArrayOfstring',
      'GetValidEffectiveDatesRequest' => '\\GetValidEffectiveDatesRequest',
      'Credentials' => '\\Credentials',
      'GetValidEffectiveDatesResponse' => '\\GetValidEffectiveDatesResponse',
      'ArrayOfProductEffectiveDates' => '\\ArrayOfProductEffectiveDates',
      'ProductEffectiveDates' => '\\ProductEffectiveDates',
      'ErrorInformation' => '\\ErrorInformation',
      'GetEffectiveDateRangeRequest' => '\\GetEffectiveDateRangeRequest',
      'GetEffectiveDateRangeResponse' => '\\GetEffectiveDateRangeResponse',
      'GetStateRequest' => '\\GetStateRequest',
      'GetStateResponse' => '\\GetStateResponse',
      'RatePlansRequest' => '\\RatePlansRequest',
      'ArrayOfApplicant' => '\\ArrayOfApplicant',
      'Applicant' => '\\Applicant',
      'ArrayOfProductTypeId' => '\\ArrayOfProductTypeId',
      'RatePlansResponse' => '\\RatePlansResponse',
      'ArrayOfRatedPlan' => '\\ArrayOfRatedPlan',
      'RatedPlan' => '\\RatedPlan',
      'ArrayOfBenefit' => '\\ArrayOfBenefit',
      'Benefit' => '\\Benefit',
      'ArrayOfMemberRate' => '\\ArrayOfMemberRate',
      'MemberRate' => '\\MemberRate',
      'ArrayOfRider' => '\\ArrayOfRider',
      'Rider' => '\\Rider',
      'GetTermsRequest' => '\\GetTermsRequest',
      'GetTermsResponse' => '\\GetTermsResponse',
      'ArrayOfTerm' => '\\ArrayOfTerm',
      'Term' => '\\Term',
      'RateCartRequest' => '\\RateCartRequest',
      'RateCartResponse' => '\\RateCartResponse',
      'GetValidEffectiveDates' => '\\GetValidEffectiveDates',
      'GetEffectiveDateRange' => '\\GetEffectiveDateRange',
      'GetState' => '\\GetState',
      'RatePlans' => '\\RatePlans',
      'GetSTMTerms' => '\\GetSTMTerms',
      'GetSTMTermsResponse' => '\\GetSTMTermsResponse',
      'RateCart' => '\\RateCart',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://qa1-quotingbroker.ngic.com/QuotingBroker.svc?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetValidEffectiveDates $parameters
     * @return GetValidEffectiveDatesResponse
     */
    public function GetValidEffectiveDates(GetValidEffectiveDates $parameters)
    {
      return $this->__soapCall('GetValidEffectiveDates', array($parameters));
    }

    /**
     * @param GetEffectiveDateRange $parameters
     * @return GetEffectiveDateRangeResponse
     */
    public function GetEffectiveDateRange(GetEffectiveDateRange $parameters)
    {
      return $this->__soapCall('GetEffectiveDateRange', array($parameters));
    }

    /**
     * @param GetState $parameters
     * @return GetStateResponse
     */
    public function GetState(GetState $parameters)
    {
      return $this->__soapCall('GetState', array($parameters));
    }

    /**
     * @param RatePlans $parameters
     * @return RatePlansResponse
     */
    public function RatePlans(RatePlans $parameters)
    {
      return $this->__soapCall('RatePlans', array($parameters));
    }

    /**
     * @param GetSTMTerms $parameters
     * @return GetSTMTermsResponse
     */
    public function GetSTMTerms(GetSTMTerms $parameters)
    {
      return $this->__soapCall('GetSTMTerms', array($parameters));
    }

    /**
     * @param RateCart $parameters
     * @return RateCartResponse
     */
    public function RateCart(RateCart $parameters)
    {
      return $this->__soapCall('RateCart', array($parameters));
    }

}
