<?php

class MemberRate
{

    /**
     * @var string $MemberId
     */
    protected $MemberId = null;

    /**
     * @var float $Rate
     */
    protected $Rate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getMemberId()
    {
      return $this->MemberId;
    }

    /**
     * @param string $MemberId
     * @return MemberRate
     */
    public function setMemberId($MemberId)
    {
      $this->MemberId = $MemberId;
      return $this;
    }

    /**
     * @return float
     */
    public function getRate()
    {
      return $this->Rate;
    }

    /**
     * @param float $Rate
     * @return MemberRate
     */
    public function setRate($Rate)
    {
      $this->Rate = $Rate;
      return $this;
    }

}
