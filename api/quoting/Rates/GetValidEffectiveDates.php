<?php

class GetValidEffectiveDates
{

    /**
     * @var GetValidEffectiveDatesRequest $request
     */
    protected $request = null;

    /**
     * @param GetValidEffectiveDatesRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return GetValidEffectiveDatesRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GetValidEffectiveDatesRequest $request
     * @return GetValidEffectiveDates
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
