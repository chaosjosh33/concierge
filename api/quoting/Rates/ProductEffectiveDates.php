<?php

class ProductEffectiveDates
{

    /**
     * @var boolean $DailyEffectiveDates
     */
    protected $DailyEffectiveDates = null;

    /**
     * @var \DateTime $FirstAvailableEffectiveDate
     */
    protected $FirstAvailableEffectiveDate = null;

    /**
     * @var \DateTime $LastAvailableEffectiveDate
     */
    protected $LastAvailableEffectiveDate = null;

    /**
     * @var int $ProductType
     */
    protected $ProductType = null;

    /**
     * @var ArrayOfdateTime $ValidEffectiveDates
     */
    protected $ValidEffectiveDates = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getDailyEffectiveDates()
    {
      return $this->DailyEffectiveDates;
    }

    /**
     * @param boolean $DailyEffectiveDates
     * @return ProductEffectiveDates
     */
    public function setDailyEffectiveDates($DailyEffectiveDates)
    {
      $this->DailyEffectiveDates = $DailyEffectiveDates;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFirstAvailableEffectiveDate()
    {
      if ($this->FirstAvailableEffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->FirstAvailableEffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $FirstAvailableEffectiveDate
     * @return ProductEffectiveDates
     */
    public function setFirstAvailableEffectiveDate(\DateTime $FirstAvailableEffectiveDate = null)
    {
      if ($FirstAvailableEffectiveDate == null) {
       $this->FirstAvailableEffectiveDate = null;
      } else {
        $this->FirstAvailableEffectiveDate = $FirstAvailableEffectiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastAvailableEffectiveDate()
    {
      if ($this->LastAvailableEffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->LastAvailableEffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $LastAvailableEffectiveDate
     * @return ProductEffectiveDates
     */
    public function setLastAvailableEffectiveDate(\DateTime $LastAvailableEffectiveDate = null)
    {
      if ($LastAvailableEffectiveDate == null) {
       $this->LastAvailableEffectiveDate = null;
      } else {
        $this->LastAvailableEffectiveDate = $LastAvailableEffectiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getProductType()
    {
      return $this->ProductType;
    }

    /**
     * @param int $ProductType
     * @return ProductEffectiveDates
     */
    public function setProductType($ProductType)
    {
      $this->ProductType = $ProductType;
      return $this;
    }

    /**
     * @return ArrayOfdateTime
     */
    public function getValidEffectiveDates()
    {
      return $this->ValidEffectiveDates;
    }

    /**
     * @param ArrayOfdateTime $ValidEffectiveDates
     * @return ProductEffectiveDates
     */
    public function setValidEffectiveDates($ValidEffectiveDates)
    {
      $this->ValidEffectiveDates = $ValidEffectiveDates;
      return $this;
    }

}
