<?php

class GetStateResponse
{

    /**
     * @var GetStateResponse $GetStateResult
     */
    protected $GetStateResult = null;

    /**
     * @param GetStateResponse $GetStateResult
     */
    public function __construct($GetStateResult)
    {
      $this->GetStateResult = $GetStateResult;
    }

    /**
     * @return GetStateResponse
     */
    public function getGetStateResult()
    {
      return $this->GetStateResult;
    }

    /**
     * @param GetStateResponse $GetStateResult
     * @return GetStateResponse
     */
    public function setGetStateResult($GetStateResult)
    {
      $this->GetStateResult = $GetStateResult;
      return $this;
    }

}
