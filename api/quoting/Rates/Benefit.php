<?php

class Benefit
{

    /**
     * @var string $FormattedValue
     */
    protected $FormattedValue = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var float $Premium
     */
    protected $Premium = null;

    /**
     * @var int $Value
     */
    protected $Value = null;

    /**
     * @var BenefitType $Which
     */
    protected $Which = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getFormattedValue()
    {
      return $this->FormattedValue;
    }

    /**
     * @param string $FormattedValue
     * @return Benefit
     */
    public function setFormattedValue($FormattedValue)
    {
      $this->FormattedValue = $FormattedValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return Benefit
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return float
     */
    public function getPremium()
    {
      return $this->Premium;
    }

    /**
     * @param float $Premium
     * @return Benefit
     */
    public function setPremium($Premium)
    {
      $this->Premium = $Premium;
      return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param int $Value
     * @return Benefit
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

    /**
     * @return BenefitType
     */
    public function getWhich()
    {
      return $this->Which;
    }

    /**
     * @param BenefitType $Which
     * @return Benefit
     */
    public function setWhich($Which)
    {
      $this->Which = $Which;
      return $this;
    }

}
