<?php

class RatePlansRequest
{

    /**
     * @var ArrayOfApplicant $Applicants
     */
    protected $Applicants = null;

    /**
     * @var string $BrokerId
     */
    protected $BrokerId = null;

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var \DateTime $EffectiveDate
     */
    protected $EffectiveDate = null;

    /**
     * @var ArrayOfProductTypeId $ProductTypesFilter
     */
    protected $ProductTypesFilter = null;

    /**
     * @var string $ZipCode
     */
    protected $ZipCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfApplicant
     */
    public function getApplicants()
    {
      return $this->Applicants;
    }

    /**
     * @param ArrayOfApplicant $Applicants
     * @return RatePlansRequest
     */
    public function setApplicants($Applicants)
    {
      $this->Applicants = $Applicants;
      return $this;
    }

    /**
     * @return string
     */
    public function getBrokerId()
    {
      return $this->BrokerId;
    }

    /**
     * @param string $BrokerId
     * @return RatePlansRequest
     */
    public function setBrokerId($BrokerId)
    {
      $this->BrokerId = $BrokerId;
      return $this;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return RatePlansRequest
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
      if ($this->EffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EffectiveDate
     * @return RatePlansRequest
     */
    public function setEffectiveDate(\DateTime $EffectiveDate = null)
    {
      if ($EffectiveDate == null) {
       $this->EffectiveDate = null;
      } else {
        $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return ArrayOfProductTypeId
     */
    public function getProductTypesFilter()
    {
      return $this->ProductTypesFilter;
    }

    /**
     * @param ArrayOfProductTypeId $ProductTypesFilter
     * @return RatePlansRequest
     */
    public function setProductTypesFilter($ProductTypesFilter)
    {
      $this->ProductTypesFilter = $ProductTypesFilter;
      return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
      return $this->ZipCode;
    }

    /**
     * @param string $ZipCode
     * @return RatePlansRequest
     */
    public function setZipCode($ZipCode)
    {
      $this->ZipCode = $ZipCode;
      return $this;
    }

}
