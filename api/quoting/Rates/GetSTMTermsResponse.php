<?php

class GetSTMTermsResponse
{

    /**
     * @var GetTermsResponse $GetSTMTermsResult
     */
    protected $GetSTMTermsResult = null;

    /**
     * @param GetTermsResponse $GetSTMTermsResult
     */
    public function __construct($GetSTMTermsResult)
    {
      $this->GetSTMTermsResult = $GetSTMTermsResult;
    }

    /**
     * @return GetTermsResponse
     */
    public function getGetSTMTermsResult()
    {
      return $this->GetSTMTermsResult;
    }

    /**
     * @param GetTermsResponse $GetSTMTermsResult
     * @return GetSTMTermsResponse
     */
    public function setGetSTMTermsResult($GetSTMTermsResult)
    {
      $this->GetSTMTermsResult = $GetSTMTermsResult;
      return $this;
    }

}
