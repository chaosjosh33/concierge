<?php

class Rider
{

    /**
     * @var string $Code
     */
    protected $Code = null;

    /**
     * @var string $E123ProductSubCode
     */
    protected $E123ProductSubCode = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param string $Code
     * @return Rider
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return string
     */
    public function getE123ProductSubCode()
    {
      return $this->E123ProductSubCode;
    }

    /**
     * @param string $E123ProductSubCode
     * @return Rider
     */
    public function setE123ProductSubCode($E123ProductSubCode)
    {
      $this->E123ProductSubCode = $E123ProductSubCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return Rider
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

}
