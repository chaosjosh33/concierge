<?php

class ArrayOfMemberRate implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var MemberRate[] $MemberRate
     */
    protected $MemberRate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return MemberRate[]
     */
    public function getMemberRate()
    {
      return $this->MemberRate;
    }

    /**
     * @param MemberRate[] $MemberRate
     * @return ArrayOfMemberRate
     */
    public function setMemberRate(array $MemberRate = null)
    {
      $this->MemberRate = $MemberRate;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->MemberRate[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return MemberRate
     */
    public function offsetGet($offset)
    {
      return $this->MemberRate[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param MemberRate $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->MemberRate[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->MemberRate[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return MemberRate Return the current element
     */
    public function current()
    {
      return current($this->MemberRate);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->MemberRate);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->MemberRate);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->MemberRate);
    }

    /**
     * Countable implementation
     *
     * @return MemberRate Return count of elements
     */
    public function count()
    {
      return count($this->MemberRate);
    }

}
