<?php

class GetEffectiveDateRangeResponse
{

    /**
     * @var GetEffectiveDateRangeResponse $GetEffectiveDateRangeResult
     */
    protected $GetEffectiveDateRangeResult = null;

    /**
     * @param GetEffectiveDateRangeResponse $GetEffectiveDateRangeResult
     */
    public function __construct($GetEffectiveDateRangeResult)
    {
      $this->GetEffectiveDateRangeResult = $GetEffectiveDateRangeResult;
    }

    /**
     * @return GetEffectiveDateRangeResponse
     */
    public function getGetEffectiveDateRangeResult()
    {
      return $this->GetEffectiveDateRangeResult;
    }

    /**
     * @param GetEffectiveDateRangeResponse $GetEffectiveDateRangeResult
     * @return GetEffectiveDateRangeResponse
     */
    public function setGetEffectiveDateRangeResult($GetEffectiveDateRangeResult)
    {
      $this->GetEffectiveDateRangeResult = $GetEffectiveDateRangeResult;
      return $this;
    }

}
