<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../init.php';
$thingEngine->slim->config(array(
	'templates.path' => __DIR__
));
$thingEngine->slim->map('/', function () use ($thingEngine) {
	/*
$generator = new \Wsdl2PhpGenerator\Generator();
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => 'https://qa1-enrollmentservice.ngic.com/2016/05/EnrollmentManager.svc?wsdl',
        'outputDir' => __DIR__.'/Library'
    ))
);
	*/
	require_once __DIR__.'/Rates/autoload.php';
	//require_once __DIR__.'/Library/autoload.php';
	$request = new RateCartRequest();
	$login = new Credentials();
	$login->setUserId('TestUser');
	$login->setPassword('Test1234');
	$request->setCredentials($login);
	$request->setBrokerId('186598');
	$datetime = new DateTime('tomorrow');
	$datetime->modify('+8 days');
	$request->setEffectiveDate($datetime);
	$request->setOverWriteCartPlans(false);
	$request->setZipCode('53211');
	$filter = array('STM','NHICSupplemental','StarmountDental','TIC','Association');
	$filt= new ArrayOfProductTypeId();
	$filt->setProductTypeId($filter);
	$request->setProductTypesFilter($filt);
	$pl = array();
	$pls = new RatedPlan($pl);
	$request->setPlansToRate($pls);
	$applicant = new Applicant();
	$dob = new DateTime("1999-01-10");
	$applicant->setDOB($dob);  // Date of Birth mm/dd/yyyy
	$applicant->setGender('1');
	$applicant->setRelationshipType(1);  // {1=Primary,2=Spouse,3=Dependent}
	$applicant->setSmoker(true);   // {True,False}
	$apps[]=$applicant;
	$applicants = new ArrayOfApplicant();
	$applicants->setApplicant($apps);
	$request->setApplicants($applicants);
	echo "<PRE style='background-color:#eaeaea'>";
	//print_r($request);
	echo "</PRE>";
	echo "<PRE>";
	$apiURL = 'https://qa1-quotingbroker.ngic.com/QuotingBroker.svc';
	$soapOptions = array(
		'exceptions'          => 1,
		'trace'               => 1,
		'connection_timeout'  => 2000,
		'location' => 'https://qa1-quotingbroker.ngic.com/QuotingBroker.svc',
		'local_cert'     => __DIR__."concierge.crt",
		'ssl_method' => 'SOAP_SSL_METHOD_TLS'
	);
	$soapClient = new QuotingBroker($soapOptions);
	//$soapClient->ErrorInformation($request);
	//print_r($soapClient);exit();
	$rateplans = new RateCart($request);
	try {
		//$plans = new RatePlans($request);
		$plans = $soapClient->RateCart($rateplans);
		print_r($plans);
	} catch (Exception $e) {
		print_r($e);
	}
})->via('GET','POST');
$thingEngine->slim->run();