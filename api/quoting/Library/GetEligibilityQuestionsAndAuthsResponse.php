<?php

class GetEligibilityQuestionsAndAuthsResponse
{

    /**
     * @var GetEligibilityResponse $GetEligibilityQuestionsAndAuthsResult
     */
    protected $GetEligibilityQuestionsAndAuthsResult = null;

    /**
     * @param GetEligibilityResponse $GetEligibilityQuestionsAndAuthsResult
     */
    public function __construct($GetEligibilityQuestionsAndAuthsResult)
    {
      $this->GetEligibilityQuestionsAndAuthsResult = $GetEligibilityQuestionsAndAuthsResult;
    }

    /**
     * @return GetEligibilityResponse
     */
    public function getGetEligibilityQuestionsAndAuthsResult()
    {
      return $this->GetEligibilityQuestionsAndAuthsResult;
    }

    /**
     * @param GetEligibilityResponse $GetEligibilityQuestionsAndAuthsResult
     * @return GetEligibilityQuestionsAndAuthsResponse
     */
    public function setGetEligibilityQuestionsAndAuthsResult($GetEligibilityQuestionsAndAuthsResult)
    {
      $this->GetEligibilityQuestionsAndAuthsResult = $GetEligibilityQuestionsAndAuthsResult;
      return $this;
    }

}
