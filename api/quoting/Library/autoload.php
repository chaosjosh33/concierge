<?php


 function autoload_c904d803d74f9cec30a01a80ccba7d9a($class)
{
    $classes = array(
        'EnrollmentManager' => __DIR__ .'/EnrollmentManager.php',
        'ArrayOfstring' => __DIR__ .'/ArrayOfstring.php',
        'Credentials' => __DIR__ .'/Credentials.php',
        'ArrayOfProductApplication' => __DIR__ .'/ArrayOfProductApplication.php',
        'ProductApplication' => __DIR__ .'/ProductApplication.php',
        'EnumerationsApplicationType' => __DIR__ .'/EnumerationsApplicationType.php',
        'ArrayOfQuestion' => __DIR__ .'/ArrayOfQuestion.php',
        'Question' => __DIR__ .'/Question.php',
        'ArrayOfPossibleAnswer' => __DIR__ .'/ArrayOfPossibleAnswer.php',
        'PossibleAnswer' => __DIR__ .'/PossibleAnswer.php',
        'EnumerationsAnswerType' => __DIR__ .'/EnumerationsAnswerType.php',
        'EnumerationsQuestionType' => __DIR__ .'/EnumerationsQuestionType.php',
        'EnumerationsBaseProduct' => __DIR__ .'/EnumerationsBaseProduct.php',
        'ArrayOfValidationError' => __DIR__ .'/ArrayOfValidationError.php',
        'ValidationError' => __DIR__ .'/ValidationError.php',
        'Beneficiary' => __DIR__ .'/Beneficiary.php',
        'Demographics' => __DIR__ .'/Demographics.php',
        'ArrayOfApplicant' => __DIR__ .'/ArrayOfApplicant.php',
        'Applicant' => __DIR__ .'/Applicant.php',
        'Partner' => __DIR__ .'/Partner.php',
        'Payment' => __DIR__ .'/Payment.php',
        'ArrayOfQuestionResponse' => __DIR__ .'/ArrayOfQuestionResponse.php',
        'QuestionResponse' => __DIR__ .'/QuestionResponse.php',
        'ArrayOfCoverage' => __DIR__ .'/ArrayOfCoverage.php',
        'Coverage' => __DIR__ .'/Coverage.php',
        'ArrayOfSubmissionResult' => __DIR__ .'/ArrayOfSubmissionResult.php',
        'SubmissionResult' => __DIR__ .'/SubmissionResult.php',
        'GetEligibilityRequest' => __DIR__ .'/GetEligibilityRequest.php',
        'GetEligibilityResponse' => __DIR__ .'/GetEligibilityResponse.php',
        'SubmitEnrollmentRequest' => __DIR__ .'/SubmitEnrollmentRequest.php',
        'SubmitEnrollmentResponse' => __DIR__ .'/SubmitEnrollmentResponse.php',
        'GetEligibilityQuestionsAndAuths' => __DIR__ .'/GetEligibilityQuestionsAndAuths.php',
        'GetEligibilityQuestionsAndAuthsResponse' => __DIR__ .'/GetEligibilityQuestionsAndAuthsResponse.php',
        'SubmitEnrollment' => __DIR__ .'/SubmitEnrollment.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_c904d803d74f9cec30a01a80ccba7d9a');

// Do nothing. The rest is just leftovers from the code generation.
{
}
