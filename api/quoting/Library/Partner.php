<?php

class Partner
{

    /**
     * @var string $AgentNumber
     */
    protected $AgentNumber = null;

    /**
     * @var string $ClientCaseID
     */
    protected $ClientCaseID = null;

    /**
     * @var string $ConfirmationURL
     */
    protected $ConfirmationURL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAgentNumber()
    {
      return $this->AgentNumber;
    }

    /**
     * @param string $AgentNumber
     * @return Partner
     */
    public function setAgentNumber($AgentNumber)
    {
      $this->AgentNumber = $AgentNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getClientCaseID()
    {
      return $this->ClientCaseID;
    }

    /**
     * @param string $ClientCaseID
     * @return Partner
     */
    public function setClientCaseID($ClientCaseID)
    {
      $this->ClientCaseID = $ClientCaseID;
      return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationURL()
    {
      return $this->ConfirmationURL;
    }

    /**
     * @param string $ConfirmationURL
     * @return Partner
     */
    public function setConfirmationURL($ConfirmationURL)
    {
      $this->ConfirmationURL = $ConfirmationURL;
      return $this;
    }

}
