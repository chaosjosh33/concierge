<?php

class Payment
{

    /**
     * @var string $AccountHolderFirstName
     */
    protected $AccountHolderFirstName = null;

    /**
     * @var string $AccountHolderLastName
     */
    protected $AccountHolderLastName = null;

    /**
     * @var string $AccountNumber
     */
    protected $AccountNumber = null;

    /**
     * @var int $AccountType
     */
    protected $AccountType = null;

    /**
     * @var int $BankDraft
     */
    protected $BankDraft = null;

    /**
     * @var string $BankName
     */
    protected $BankName = null;

    /**
     * @var string $CVV
     */
    protected $CVV = null;

    /**
     * @var string $CreditCardNumber
     */
    protected $CreditCardNumber = null;

    /**
     * @var int $ExpirationMonth
     */
    protected $ExpirationMonth = null;

    /**
     * @var int $ExpirationYear
     */
    protected $ExpirationYear = null;

    /**
     * @var string $RoutingNumber
     */
    protected $RoutingNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountHolderFirstName()
    {
      return $this->AccountHolderFirstName;
    }

    /**
     * @param string $AccountHolderFirstName
     * @return Payment
     */
    public function setAccountHolderFirstName($AccountHolderFirstName)
    {
      $this->AccountHolderFirstName = $AccountHolderFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountHolderLastName()
    {
      return $this->AccountHolderLastName;
    }

    /**
     * @param string $AccountHolderLastName
     * @return Payment
     */
    public function setAccountHolderLastName($AccountHolderLastName)
    {
      $this->AccountHolderLastName = $AccountHolderLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
      return $this->AccountNumber;
    }

    /**
     * @param string $AccountNumber
     * @return Payment
     */
    public function setAccountNumber($AccountNumber)
    {
      $this->AccountNumber = $AccountNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getAccountType()
    {
      return $this->AccountType;
    }

    /**
     * @param int $AccountType
     * @return Payment
     */
    public function setAccountType($AccountType)
    {
      $this->AccountType = $AccountType;
      return $this;
    }

    /**
     * @return int
     */
    public function getBankDraft()
    {
      return $this->BankDraft;
    }

    /**
     * @param int $BankDraft
     * @return Payment
     */
    public function setBankDraft($BankDraft)
    {
      $this->BankDraft = $BankDraft;
      return $this;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
      return $this->BankName;
    }

    /**
     * @param string $BankName
     * @return Payment
     */
    public function setBankName($BankName)
    {
      $this->BankName = $BankName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCVV()
    {
      return $this->CVV;
    }

    /**
     * @param string $CVV
     * @return Payment
     */
    public function setCVV($CVV)
    {
      $this->CVV = $CVV;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreditCardNumber()
    {
      return $this->CreditCardNumber;
    }

    /**
     * @param string $CreditCardNumber
     * @return Payment
     */
    public function setCreditCardNumber($CreditCardNumber)
    {
      $this->CreditCardNumber = $CreditCardNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getExpirationMonth()
    {
      return $this->ExpirationMonth;
    }

    /**
     * @param int $ExpirationMonth
     * @return Payment
     */
    public function setExpirationMonth($ExpirationMonth)
    {
      $this->ExpirationMonth = $ExpirationMonth;
      return $this;
    }

    /**
     * @return int
     */
    public function getExpirationYear()
    {
      return $this->ExpirationYear;
    }

    /**
     * @param int $ExpirationYear
     * @return Payment
     */
    public function setExpirationYear($ExpirationYear)
    {
      $this->ExpirationYear = $ExpirationYear;
      return $this;
    }

    /**
     * @return string
     */
    public function getRoutingNumber()
    {
      return $this->RoutingNumber;
    }

    /**
     * @param string $RoutingNumber
     * @return Payment
     */
    public function setRoutingNumber($RoutingNumber)
    {
      $this->RoutingNumber = $RoutingNumber;
      return $this;
    }

}
