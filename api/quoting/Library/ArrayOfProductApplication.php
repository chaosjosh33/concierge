<?php

class ArrayOfProductApplication implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductApplication[] $ProductApplication
     */
    protected $ProductApplication = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductApplication[]
     */
    public function getProductApplication()
    {
      return $this->ProductApplication;
    }

    /**
     * @param ProductApplication[] $ProductApplication
     * @return ArrayOfProductApplication
     */
    public function setProductApplication(array $ProductApplication = null)
    {
      $this->ProductApplication = $ProductApplication;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductApplication[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductApplication
     */
    public function offsetGet($offset)
    {
      return $this->ProductApplication[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductApplication $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->ProductApplication[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductApplication[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductApplication Return the current element
     */
    public function current()
    {
      return current($this->ProductApplication);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductApplication);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductApplication);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductApplication);
    }

    /**
     * Countable implementation
     *
     * @return ProductApplication Return count of elements
     */
    public function count()
    {
      return count($this->ProductApplication);
    }

}
