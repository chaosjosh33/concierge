<?php

class GetEligibilityRequest
{

    /**
     * @var string $AgentNumber
     */
    protected $AgentNumber = null;

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var ArrayOfstring $PlanIDList
     */
    protected $PlanIDList = null;

    /**
     * @var ArrayOfstring $PlanKeyList
     */
    protected $PlanKeyList = null;

    /**
     * @var string $State
     */
    protected $State = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAgentNumber()
    {
      return $this->AgentNumber;
    }

    /**
     * @param string $AgentNumber
     * @return GetEligibilityRequest
     */
    public function setAgentNumber($AgentNumber)
    {
      $this->AgentNumber = $AgentNumber;
      return $this;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return GetEligibilityRequest
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getPlanIDList()
    {
      return $this->PlanIDList;
    }

    /**
     * @param ArrayOfstring $PlanIDList
     * @return GetEligibilityRequest
     */
    public function setPlanIDList($PlanIDList)
    {
      $this->PlanIDList = $PlanIDList;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getPlanKeyList()
    {
      return $this->PlanKeyList;
    }

    /**
     * @param ArrayOfstring $PlanKeyList
     * @return GetEligibilityRequest
     */
    public function setPlanKeyList($PlanKeyList)
    {
      $this->PlanKeyList = $PlanKeyList;
      return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
      return $this->State;
    }

    /**
     * @param string $State
     * @return GetEligibilityRequest
     */
    public function setState($State)
    {
      $this->State = $State;
      return $this;
    }

}
