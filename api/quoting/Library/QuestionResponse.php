<?php

class QuestionResponse
{

    /**
     * @var int $QuestionId
     */
    protected $QuestionId = null;

    /**
     * @var string $Response
     */
    protected $Response = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
      return $this->QuestionId;
    }

    /**
     * @param int $QuestionId
     * @return QuestionResponse
     */
    public function setQuestionId($QuestionId)
    {
      $this->QuestionId = $QuestionId;
      return $this;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
      return $this->Response;
    }

    /**
     * @param string $Response
     * @return QuestionResponse
     */
    public function setResponse($Response)
    {
      $this->Response = $Response;
      return $this;
    }

}
