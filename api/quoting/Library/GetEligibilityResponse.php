<?php

class GetEligibilityResponse
{

    /**
     * @var ArrayOfProductApplication $Applications
     */
    protected $Applications = null;

    /**
     * @var string $AuthorizationText
     */
    protected $AuthorizationText = null;

    /**
     * @var ArrayOfValidationError $ValidationErrors
     */
    protected $ValidationErrors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfProductApplication
     */
    public function getApplications()
    {
      return $this->Applications;
    }

    /**
     * @param ArrayOfProductApplication $Applications
     * @return GetEligibilityResponse
     */
    public function setApplications($Applications)
    {
      $this->Applications = $Applications;
      return $this;
    }

    /**
     * @return string
     */
    public function getAuthorizationText()
    {
      return $this->AuthorizationText;
    }

    /**
     * @param string $AuthorizationText
     * @return GetEligibilityResponse
     */
    public function setAuthorizationText($AuthorizationText)
    {
      $this->AuthorizationText = $AuthorizationText;
      return $this;
    }

    /**
     * @return ArrayOfValidationError
     */
    public function getValidationErrors()
    {
      return $this->ValidationErrors;
    }

    /**
     * @param ArrayOfValidationError $ValidationErrors
     * @return GetEligibilityResponse
     */
    public function setValidationErrors($ValidationErrors)
    {
      $this->ValidationErrors = $ValidationErrors;
      return $this;
    }

}
