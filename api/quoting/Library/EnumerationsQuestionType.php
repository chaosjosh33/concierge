<?php

class EnumerationsQuestionType
{
    const __default = 'Eligibility';
    const Eligibility = 'Eligibility';
    const Authorization = 'Authorization';
    const PriorInsurance = 'PriorInsurance';
    const PreEx = 'PreEx';
    const HRF = 'HRF';
    const Rewrite = 'Rewrite';


}
