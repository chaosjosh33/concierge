<?php

class ArrayOfApplicant implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Applicant[] $Applicant
     */
    protected $Applicant = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Applicant[]
     */
    public function getApplicant()
    {
      return $this->Applicant;
    }

    /**
     * @param Applicant[] $Applicant
     * @return ArrayOfApplicant
     */
    public function setApplicant(array $Applicant = null)
    {
      $this->Applicant = $Applicant;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Applicant[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Applicant
     */
    public function offsetGet($offset)
    {
      return $this->Applicant[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Applicant $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Applicant[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Applicant[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Applicant Return the current element
     */
    public function current()
    {
      return current($this->Applicant);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Applicant);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Applicant);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Applicant);
    }

    /**
     * Countable implementation
     *
     * @return Applicant Return count of elements
     */
    public function count()
    {
      return count($this->Applicant);
    }

}
