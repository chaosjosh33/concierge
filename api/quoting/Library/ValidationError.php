<?php

class ValidationError
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorDetail
     */
    protected $ErrorDetail = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return ValidationError
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorDetail()
    {
      return $this->ErrorDetail;
    }

    /**
     * @param string $ErrorDetail
     * @return ValidationError
     */
    public function setErrorDetail($ErrorDetail)
    {
      $this->ErrorDetail = $ErrorDetail;
      return $this;
    }

}
