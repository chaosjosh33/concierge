<?php

class Question
{

    /**
     * @var string $GroupHeader
     */
    protected $GroupHeader = null;

    /**
     * @var ArrayOfPossibleAnswer $PossibleAnswers
     */
    protected $PossibleAnswers = null;

    /**
     * @var string $QuestionHeader
     */
    protected $QuestionHeader = null;

    /**
     * @var int $QuestionId
     */
    protected $QuestionId = null;

    /**
     * @var string $QuestionText
     */
    protected $QuestionText = null;

    /**
     * @var string $QuestionTitle
     */
    protected $QuestionTitle = null;

    /**
     * @var EnumerationsQuestionType $QuestionType
     */
    protected $QuestionType = null;

    /**
     * @var float $SequenceNo
     */
    protected $SequenceNo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getGroupHeader()
    {
      return $this->GroupHeader;
    }

    /**
     * @param string $GroupHeader
     * @return Question
     */
    public function setGroupHeader($GroupHeader)
    {
      $this->GroupHeader = $GroupHeader;
      return $this;
    }

    /**
     * @return ArrayOfPossibleAnswer
     */
    public function getPossibleAnswers()
    {
      return $this->PossibleAnswers;
    }

    /**
     * @param ArrayOfPossibleAnswer $PossibleAnswers
     * @return Question
     */
    public function setPossibleAnswers($PossibleAnswers)
    {
      $this->PossibleAnswers = $PossibleAnswers;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuestionHeader()
    {
      return $this->QuestionHeader;
    }

    /**
     * @param string $QuestionHeader
     * @return Question
     */
    public function setQuestionHeader($QuestionHeader)
    {
      $this->QuestionHeader = $QuestionHeader;
      return $this;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
      return $this->QuestionId;
    }

    /**
     * @param int $QuestionId
     * @return Question
     */
    public function setQuestionId($QuestionId)
    {
      $this->QuestionId = $QuestionId;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuestionText()
    {
      return $this->QuestionText;
    }

    /**
     * @param string $QuestionText
     * @return Question
     */
    public function setQuestionText($QuestionText)
    {
      $this->QuestionText = $QuestionText;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuestionTitle()
    {
      return $this->QuestionTitle;
    }

    /**
     * @param string $QuestionTitle
     * @return Question
     */
    public function setQuestionTitle($QuestionTitle)
    {
      $this->QuestionTitle = $QuestionTitle;
      return $this;
    }

    /**
     * @return EnumerationsQuestionType
     */
    public function getQuestionType()
    {
      return $this->QuestionType;
    }

    /**
     * @param EnumerationsQuestionType $QuestionType
     * @return Question
     */
    public function setQuestionType($QuestionType)
    {
      $this->QuestionType = $QuestionType;
      return $this;
    }

    /**
     * @return float
     */
    public function getSequenceNo()
    {
      return $this->SequenceNo;
    }

    /**
     * @param float $SequenceNo
     * @return Question
     */
    public function setSequenceNo($SequenceNo)
    {
      $this->SequenceNo = $SequenceNo;
      return $this;
    }

}
