<?php

class Applicant
{

    /**
     * @var \DateTime $DOB
     */
    protected $DOB = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var int $Gender
     */
    protected $Gender = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var string $MiddleInitial
     */
    protected $MiddleInitial = null;

    /**
     * @var int $Relationship
     */
    protected $Relationship = null;

    /**
     * @var string $SSN
     */
    protected $SSN = null;

    /**
     * @var boolean $Smoker
     */
    protected $Smoker = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getDOB()
    {
      if ($this->DOB == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DOB);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DOB
     * @return Applicant
     */
    public function setDOB(\DateTime $DOB = null)
    {
      if ($DOB == null) {
       $this->DOB = null;
      } else {
        $this->DOB = $DOB->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return Applicant
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return int
     */
    public function getGender()
    {
      return $this->Gender;
    }

    /**
     * @param int $Gender
     * @return Applicant
     */
    public function setGender($Gender)
    {
      $this->Gender = $Gender;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return Applicant
     */
    public function setLastName($LastName)
    {
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleInitial()
    {
      return $this->MiddleInitial;
    }

    /**
     * @param string $MiddleInitial
     * @return Applicant
     */
    public function setMiddleInitial($MiddleInitial)
    {
      $this->MiddleInitial = $MiddleInitial;
      return $this;
    }

    /**
     * @return int
     */
    public function getRelationship()
    {
      return $this->Relationship;
    }

    /**
     * @param int $Relationship
     * @return Applicant
     */
    public function setRelationship($Relationship)
    {
      $this->Relationship = $Relationship;
      return $this;
    }

    /**
     * @return string
     */
    public function getSSN()
    {
      return $this->SSN;
    }

    /**
     * @param string $SSN
     * @return Applicant
     */
    public function setSSN($SSN)
    {
      $this->SSN = $SSN;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSmoker()
    {
      return $this->Smoker;
    }

    /**
     * @param boolean $Smoker
     * @return Applicant
     */
    public function setSmoker($Smoker)
    {
      $this->Smoker = $Smoker;
      return $this;
    }

}
