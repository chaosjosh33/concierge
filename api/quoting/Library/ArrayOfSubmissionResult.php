<?php

class ArrayOfSubmissionResult implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SubmissionResult[] $SubmissionResult
     */
    protected $SubmissionResult = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SubmissionResult[]
     */
    public function getSubmissionResult()
    {
      return $this->SubmissionResult;
    }

    /**
     * @param SubmissionResult[] $SubmissionResult
     * @return ArrayOfSubmissionResult
     */
    public function setSubmissionResult(array $SubmissionResult = null)
    {
      $this->SubmissionResult = $SubmissionResult;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SubmissionResult[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SubmissionResult
     */
    public function offsetGet($offset)
    {
      return $this->SubmissionResult[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SubmissionResult $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->SubmissionResult[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SubmissionResult[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SubmissionResult Return the current element
     */
    public function current()
    {
      return current($this->SubmissionResult);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SubmissionResult);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SubmissionResult);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SubmissionResult);
    }

    /**
     * Countable implementation
     *
     * @return SubmissionResult Return count of elements
     */
    public function count()
    {
      return count($this->SubmissionResult);
    }

}
