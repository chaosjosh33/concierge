<?php

class GetEligibilityQuestionsAndAuths
{

    /**
     * @var GetEligibilityRequest $request
     */
    protected $request = null;

    /**
     * @param GetEligibilityRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return GetEligibilityRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GetEligibilityRequest $request
     * @return GetEligibilityQuestionsAndAuths
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
