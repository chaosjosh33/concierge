<?php

class Coverage
{

    /**
     * @var \DateTime $EffectiveDate
     */
    protected $EffectiveDate = null;

    /**
     * @var float $MonthlyPremium
     */
    protected $MonthlyPremium = null;

    /**
     * @var string $PlanKey
     */
    protected $PlanKey = null;

    /**
     * @var int $Term
     */
    protected $Term = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
      if ($this->EffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EffectiveDate
     * @return Coverage
     */
    public function setEffectiveDate(\DateTime $EffectiveDate = null)
    {
      if ($EffectiveDate == null) {
       $this->EffectiveDate = null;
      } else {
        $this->EffectiveDate = $EffectiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getMonthlyPremium()
    {
      return $this->MonthlyPremium;
    }

    /**
     * @param float $MonthlyPremium
     * @return Coverage
     */
    public function setMonthlyPremium($MonthlyPremium)
    {
      $this->MonthlyPremium = $MonthlyPremium;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlanKey()
    {
      return $this->PlanKey;
    }

    /**
     * @param string $PlanKey
     * @return Coverage
     */
    public function setPlanKey($PlanKey)
    {
      $this->PlanKey = $PlanKey;
      return $this;
    }

    /**
     * @return int
     */
    public function getTerm()
    {
      return $this->Term;
    }

    /**
     * @param int $Term
     * @return Coverage
     */
    public function setTerm($Term)
    {
      $this->Term = $Term;
      return $this;
    }

}
