<?php

class ArrayOfQuestionResponse implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var QuestionResponse[] $QuestionResponse
     */
    protected $QuestionResponse = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return QuestionResponse[]
     */
    public function getQuestionResponse()
    {
      return $this->QuestionResponse;
    }

    /**
     * @param QuestionResponse[] $QuestionResponse
     * @return ArrayOfQuestionResponse
     */
    public function setQuestionResponse(array $QuestionResponse = null)
    {
      $this->QuestionResponse = $QuestionResponse;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->QuestionResponse[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return QuestionResponse
     */
    public function offsetGet($offset)
    {
      return $this->QuestionResponse[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param QuestionResponse $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->QuestionResponse[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->QuestionResponse[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return QuestionResponse Return the current element
     */
    public function current()
    {
      return current($this->QuestionResponse);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->QuestionResponse);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->QuestionResponse);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->QuestionResponse);
    }

    /**
     * Countable implementation
     *
     * @return QuestionResponse Return count of elements
     */
    public function count()
    {
      return count($this->QuestionResponse);
    }

}
