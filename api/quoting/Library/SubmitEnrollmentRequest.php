<?php

class SubmitEnrollmentRequest
{

    /**
     * @var Beneficiary $Beneficiary
     */
    protected $Beneficiary = null;

    /**
     * @var Credentials $Credentials
     */
    protected $Credentials = null;

    /**
     * @var Demographics $Demographics
     */
    protected $Demographics = null;

    /**
     * @var Partner $PartnerInformation
     */
    protected $PartnerInformation = null;

    /**
     * @var Payment $PaymentInformation
     */
    protected $PaymentInformation = null;

    /**
     * @var ArrayOfQuestionResponse $QuestionResponses
     */
    protected $QuestionResponses = null;

    /**
     * @var ArrayOfCoverage $SelectedPlans
     */
    protected $SelectedPlans = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Beneficiary
     */
    public function getBeneficiary()
    {
      return $this->Beneficiary;
    }

    /**
     * @param Beneficiary $Beneficiary
     * @return SubmitEnrollmentRequest
     */
    public function setBeneficiary($Beneficiary)
    {
      $this->Beneficiary = $Beneficiary;
      return $this;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
      return $this->Credentials;
    }

    /**
     * @param Credentials $Credentials
     * @return SubmitEnrollmentRequest
     */
    public function setCredentials($Credentials)
    {
      $this->Credentials = $Credentials;
      return $this;
    }

    /**
     * @return Demographics
     */
    public function getDemographics()
    {
      return $this->Demographics;
    }

    /**
     * @param Demographics $Demographics
     * @return SubmitEnrollmentRequest
     */
    public function setDemographics($Demographics)
    {
      $this->Demographics = $Demographics;
      return $this;
    }

    /**
     * @return Partner
     */
    public function getPartnerInformation()
    {
      return $this->PartnerInformation;
    }

    /**
     * @param Partner $PartnerInformation
     * @return SubmitEnrollmentRequest
     */
    public function setPartnerInformation($PartnerInformation)
    {
      $this->PartnerInformation = $PartnerInformation;
      return $this;
    }

    /**
     * @return Payment
     */
    public function getPaymentInformation()
    {
      return $this->PaymentInformation;
    }

    /**
     * @param Payment $PaymentInformation
     * @return SubmitEnrollmentRequest
     */
    public function setPaymentInformation($PaymentInformation)
    {
      $this->PaymentInformation = $PaymentInformation;
      return $this;
    }

    /**
     * @return ArrayOfQuestionResponse
     */
    public function getQuestionResponses()
    {
      return $this->QuestionResponses;
    }

    /**
     * @param ArrayOfQuestionResponse $QuestionResponses
     * @return SubmitEnrollmentRequest
     */
    public function setQuestionResponses($QuestionResponses)
    {
      $this->QuestionResponses = $QuestionResponses;
      return $this;
    }

    /**
     * @return ArrayOfCoverage
     */
    public function getSelectedPlans()
    {
      return $this->SelectedPlans;
    }

    /**
     * @param ArrayOfCoverage $SelectedPlans
     * @return SubmitEnrollmentRequest
     */
    public function setSelectedPlans($SelectedPlans)
    {
      $this->SelectedPlans = $SelectedPlans;
      return $this;
    }

}
