<?php

class ArrayOfQuestion implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Question[] $Question
     */
    protected $Question = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Question[]
     */
    public function getQuestion()
    {
      return $this->Question;
    }

    /**
     * @param Question[] $Question
     * @return ArrayOfQuestion
     */
    public function setQuestion(array $Question = null)
    {
      $this->Question = $Question;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Question[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Question
     */
    public function offsetGet($offset)
    {
      return $this->Question[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Question $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->Question[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Question[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Question Return the current element
     */
    public function current()
    {
      return current($this->Question);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Question);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Question);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Question);
    }

    /**
     * Countable implementation
     *
     * @return Question Return count of elements
     */
    public function count()
    {
      return count($this->Question);
    }

}
