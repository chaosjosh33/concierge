<?php

class SubmitEnrollment
{

    /**
     * @var SubmitEnrollmentRequest $request
     */
    protected $request = null;

    /**
     * @param SubmitEnrollmentRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return SubmitEnrollmentRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param SubmitEnrollmentRequest $request
     * @return SubmitEnrollment
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
