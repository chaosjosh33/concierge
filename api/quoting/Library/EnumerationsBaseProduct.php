<?php

class EnumerationsBaseProduct
{
    const __default = 'AME';
    const AME = 'AME';
    const STM = 'STM';
    const CI = 'CI';
    const TICDEN = 'TICDEN';
    const AD = 'AD';
    const ADD = 'ADD';
    const SDEN = 'SDEN';
    const TICAME = 'TICAME';
    const TICCHS = 'TICCHS';
    const TICCI = 'TICCI';
    const TICAFB = 'TICAFB';


}
