<?php

class ProductApplication
{

    /**
     * @var string $AdditionalWarningText
     */
    protected $AdditionalWarningText = null;

    /**
     * @var EnumerationsApplicationType $ApplicationType
     */
    protected $ApplicationType = null;

    /**
     * @var string $AuthorizationTitle
     */
    protected $AuthorizationTitle = null;

    /**
     * @var ArrayOfQuestion $Authorizations
     */
    protected $Authorizations = null;

    /**
     * @var string $EligibilityQuestionFooter
     */
    protected $EligibilityQuestionFooter = null;

    /**
     * @var string $EligibilityQuestionHeader
     */
    protected $EligibilityQuestionHeader = null;

    /**
     * @var ArrayOfQuestion $EligibilityQuestions
     */
    protected $EligibilityQuestions = null;

    /**
     * @var string $EligibilityTitle
     */
    protected $EligibilityTitle = null;

    /**
     * @var EnumerationsBaseProduct $Product
     */
    protected $Product = null;

    /**
     * @var string $ProductId
     */
    protected $ProductId = null;

    /**
     * @var string $StateId
     */
    protected $StateId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAdditionalWarningText()
    {
      return $this->AdditionalWarningText;
    }

    /**
     * @param string $AdditionalWarningText
     * @return ProductApplication
     */
    public function setAdditionalWarningText($AdditionalWarningText)
    {
      $this->AdditionalWarningText = $AdditionalWarningText;
      return $this;
    }

    /**
     * @return EnumerationsApplicationType
     */
    public function getApplicationType()
    {
      return $this->ApplicationType;
    }

    /**
     * @param EnumerationsApplicationType $ApplicationType
     * @return ProductApplication
     */
    public function setApplicationType($ApplicationType)
    {
      $this->ApplicationType = $ApplicationType;
      return $this;
    }

    /**
     * @return string
     */
    public function getAuthorizationTitle()
    {
      return $this->AuthorizationTitle;
    }

    /**
     * @param string $AuthorizationTitle
     * @return ProductApplication
     */
    public function setAuthorizationTitle($AuthorizationTitle)
    {
      $this->AuthorizationTitle = $AuthorizationTitle;
      return $this;
    }

    /**
     * @return ArrayOfQuestion
     */
    public function getAuthorizations()
    {
      return $this->Authorizations;
    }

    /**
     * @param ArrayOfQuestion $Authorizations
     * @return ProductApplication
     */
    public function setAuthorizations($Authorizations)
    {
      $this->Authorizations = $Authorizations;
      return $this;
    }

    /**
     * @return string
     */
    public function getEligibilityQuestionFooter()
    {
      return $this->EligibilityQuestionFooter;
    }

    /**
     * @param string $EligibilityQuestionFooter
     * @return ProductApplication
     */
    public function setEligibilityQuestionFooter($EligibilityQuestionFooter)
    {
      $this->EligibilityQuestionFooter = $EligibilityQuestionFooter;
      return $this;
    }

    /**
     * @return string
     */
    public function getEligibilityQuestionHeader()
    {
      return $this->EligibilityQuestionHeader;
    }

    /**
     * @param string $EligibilityQuestionHeader
     * @return ProductApplication
     */
    public function setEligibilityQuestionHeader($EligibilityQuestionHeader)
    {
      $this->EligibilityQuestionHeader = $EligibilityQuestionHeader;
      return $this;
    }

    /**
     * @return ArrayOfQuestion
     */
    public function getEligibilityQuestions()
    {
      return $this->EligibilityQuestions;
    }

    /**
     * @param ArrayOfQuestion $EligibilityQuestions
     * @return ProductApplication
     */
    public function setEligibilityQuestions($EligibilityQuestions)
    {
      $this->EligibilityQuestions = $EligibilityQuestions;
      return $this;
    }

    /**
     * @return string
     */
    public function getEligibilityTitle()
    {
      return $this->EligibilityTitle;
    }

    /**
     * @param string $EligibilityTitle
     * @return ProductApplication
     */
    public function setEligibilityTitle($EligibilityTitle)
    {
      $this->EligibilityTitle = $EligibilityTitle;
      return $this;
    }

    /**
     * @return EnumerationsBaseProduct
     */
    public function getProduct()
    {
      return $this->Product;
    }

    /**
     * @param EnumerationsBaseProduct $Product
     * @return ProductApplication
     */
    public function setProduct($Product)
    {
      $this->Product = $Product;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductId()
    {
      return $this->ProductId;
    }

    /**
     * @param string $ProductId
     * @return ProductApplication
     */
    public function setProductId($ProductId)
    {
      $this->ProductId = $ProductId;
      return $this;
    }

    /**
     * @return string
     */
    public function getStateId()
    {
      return $this->StateId;
    }

    /**
     * @param string $StateId
     * @return ProductApplication
     */
    public function setStateId($StateId)
    {
      $this->StateId = $StateId;
      return $this;
    }

}
