<?php

class ArrayOfPossibleAnswer implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PossibleAnswer[] $PossibleAnswer
     */
    protected $PossibleAnswer = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PossibleAnswer[]
     */
    public function getPossibleAnswer()
    {
      return $this->PossibleAnswer;
    }

    /**
     * @param PossibleAnswer[] $PossibleAnswer
     * @return ArrayOfPossibleAnswer
     */
    public function setPossibleAnswer(array $PossibleAnswer = null)
    {
      $this->PossibleAnswer = $PossibleAnswer;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PossibleAnswer[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PossibleAnswer
     */
    public function offsetGet($offset)
    {
      return $this->PossibleAnswer[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PossibleAnswer $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      $this->PossibleAnswer[$offset] = $value;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PossibleAnswer[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PossibleAnswer Return the current element
     */
    public function current()
    {
      return current($this->PossibleAnswer);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PossibleAnswer);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PossibleAnswer);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PossibleAnswer);
    }

    /**
     * Countable implementation
     *
     * @return PossibleAnswer Return count of elements
     */
    public function count()
    {
      return count($this->PossibleAnswer);
    }

}
