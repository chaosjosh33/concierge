<?php

class EnrollmentManager extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'ArrayOfstring' => '\\ArrayOfstring',
      'Credentials' => '\\Credentials',
      'ArrayOfProductApplication' => '\\ArrayOfProductApplication',
      'ProductApplication' => '\\ProductApplication',
      'ArrayOfQuestion' => '\\ArrayOfQuestion',
      'Question' => '\\Question',
      'ArrayOfPossibleAnswer' => '\\ArrayOfPossibleAnswer',
      'PossibleAnswer' => '\\PossibleAnswer',
      'ArrayOfValidationError' => '\\ArrayOfValidationError',
      'ValidationError' => '\\ValidationError',
      'Beneficiary' => '\\Beneficiary',
      'Demographics' => '\\Demographics',
      'ArrayOfApplicant' => '\\ArrayOfApplicant',
      'Applicant' => '\\Applicant',
      'Partner' => '\\Partner',
      'Payment' => '\\Payment',
      'ArrayOfQuestionResponse' => '\\ArrayOfQuestionResponse',
      'QuestionResponse' => '\\QuestionResponse',
      'ArrayOfCoverage' => '\\ArrayOfCoverage',
      'Coverage' => '\\Coverage',
      'ArrayOfSubmissionResult' => '\\ArrayOfSubmissionResult',
      'SubmissionResult' => '\\SubmissionResult',
      'GetEligibilityRequest' => '\\GetEligibilityRequest',
      'GetEligibilityResponse' => '\\GetEligibilityResponse',
      'SubmitEnrollmentRequest' => '\\SubmitEnrollmentRequest',
      'SubmitEnrollmentResponse' => '\\SubmitEnrollmentResponse',
      'GetEligibilityQuestionsAndAuths' => '\\GetEligibilityQuestionsAndAuths',
      'GetEligibilityQuestionsAndAuthsResponse' => '\\GetEligibilityQuestionsAndAuthsResponse',
      'SubmitEnrollment' => '\\SubmitEnrollment',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://qa1-enrollmentservice.ngic.com/2016/05/EnrollmentManager.svc?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetEligibilityQuestionsAndAuths $parameters
     * @return GetEligibilityQuestionsAndAuthsResponse
     */
    public function GetEligibilityQuestionsAndAuths(GetEligibilityQuestionsAndAuths $parameters)
    {
      return $this->__soapCall('GetEligibilityQuestionsAndAuths', array($parameters));
    }

    /**
     * @param SubmitEnrollment $parameters
     * @return SubmitEnrollmentResponse
     */
    public function SubmitEnrollment(SubmitEnrollment $parameters)
    {
      return $this->__soapCall('SubmitEnrollment', array($parameters));
    }

}
