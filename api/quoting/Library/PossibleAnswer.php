<?php

class PossibleAnswer
{

    /**
     * @var string $AnswerText
     */
    protected $AnswerText = null;

    /**
     * @var EnumerationsAnswerType $AnswerType
     */
    protected $AnswerType = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var boolean $IsKnockOut
     */
    protected $IsKnockOut = null;

    /**
     * @var string $KnockoutText
     */
    protected $KnockoutText = null;

    /**
     * @var int $QuestionId
     */
    protected $QuestionId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAnswerText()
    {
      return $this->AnswerText;
    }

    /**
     * @param string $AnswerText
     * @return PossibleAnswer
     */
    public function setAnswerText($AnswerText)
    {
      $this->AnswerText = $AnswerText;
      return $this;
    }

    /**
     * @return EnumerationsAnswerType
     */
    public function getAnswerType()
    {
      return $this->AnswerType;
    }

    /**
     * @param EnumerationsAnswerType $AnswerType
     * @return PossibleAnswer
     */
    public function setAnswerType($AnswerType)
    {
      $this->AnswerType = $AnswerType;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return PossibleAnswer
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return PossibleAnswer
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsKnockOut()
    {
      return $this->IsKnockOut;
    }

    /**
     * @param boolean $IsKnockOut
     * @return PossibleAnswer
     */
    public function setIsKnockOut($IsKnockOut)
    {
      $this->IsKnockOut = $IsKnockOut;
      return $this;
    }

    /**
     * @return string
     */
    public function getKnockoutText()
    {
      return $this->KnockoutText;
    }

    /**
     * @param string $KnockoutText
     * @return PossibleAnswer
     */
    public function setKnockoutText($KnockoutText)
    {
      $this->KnockoutText = $KnockoutText;
      return $this;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
      return $this->QuestionId;
    }

    /**
     * @param int $QuestionId
     * @return PossibleAnswer
     */
    public function setQuestionId($QuestionId)
    {
      $this->QuestionId = $QuestionId;
      return $this;
    }

}
