<?php

class SubmitEnrollmentResponse
{

    /**
     * @var SubmitEnrollmentResponse $SubmitEnrollmentResult
     */
    protected $SubmitEnrollmentResult = null;

    /**
     * @param SubmitEnrollmentResponse $SubmitEnrollmentResult
     */
    public function __construct($SubmitEnrollmentResult)
    {
      $this->SubmitEnrollmentResult = $SubmitEnrollmentResult;
    }

    /**
     * @return SubmitEnrollmentResponse
     */
    public function getSubmitEnrollmentResult()
    {
      return $this->SubmitEnrollmentResult;
    }

    /**
     * @param SubmitEnrollmentResponse $SubmitEnrollmentResult
     * @return SubmitEnrollmentResponse
     */
    public function setSubmitEnrollmentResult($SubmitEnrollmentResult)
    {
      $this->SubmitEnrollmentResult = $SubmitEnrollmentResult;
      return $this;
    }

}
