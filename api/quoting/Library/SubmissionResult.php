<?php

class SubmissionResult
{

    /**
     * @var int $PlanType
     */
    protected $PlanType = null;

    /**
     * @var string $PolicyNo
     */
    protected $PolicyNo = null;

    /**
     * @var ArrayOfstring $SubmissionErrors
     */
    protected $SubmissionErrors = null;

    /**
     * @var boolean $SubmissionReceived
     */
    protected $SubmissionReceived = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getPlanType()
    {
      return $this->PlanType;
    }

    /**
     * @param int $PlanType
     * @return SubmissionResult
     */
    public function setPlanType($PlanType)
    {
      $this->PlanType = $PlanType;
      return $this;
    }

    /**
     * @return string
     */
    public function getPolicyNo()
    {
      return $this->PolicyNo;
    }

    /**
     * @param string $PolicyNo
     * @return SubmissionResult
     */
    public function setPolicyNo($PolicyNo)
    {
      $this->PolicyNo = $PolicyNo;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getSubmissionErrors()
    {
      return $this->SubmissionErrors;
    }

    /**
     * @param ArrayOfstring $SubmissionErrors
     * @return SubmissionResult
     */
    public function setSubmissionErrors($SubmissionErrors)
    {
      $this->SubmissionErrors = $SubmissionErrors;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSubmissionReceived()
    {
      return $this->SubmissionReceived;
    }

    /**
     * @param boolean $SubmissionReceived
     * @return SubmissionResult
     */
    public function setSubmissionReceived($SubmissionReceived)
    {
      $this->SubmissionReceived = $SubmissionReceived;
      return $this;
    }

}
