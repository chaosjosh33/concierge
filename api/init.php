<?php
session_start();

// Attempt to Read JSON Posts
try {
    if((!empty($_SERVER["REQUEST_METHOD"])) && (!empty($_SERVER["CONTENT_TYPE"])) && ($_SERVER["REQUEST_METHOD"] == "POST") && ($_SERVER["CONTENT_TYPE"] == "application/json"))
    {
        $json = file_get_contents("php://input");
        //convert the string of data to an array
        $data = json_decode($json, true);
        if(!empty($data)){
            foreach($data as $dataKey=>$dataVal){
                $_REQUEST[$dataKey] = $dataVal;
                $_POST[$dataKey] = $dataVal;
            }
        }
    }
} catch (Exception $e) {
    
}

// Load Composer Includes
require_once __DIR__.'/../vendor/autoload.php';
// Load libraries 
require_once __DIR__.'/../libraries/redbean/rb.php';
// Load current config file
require_once 'config.php';



// Vefify API KEYS are Set
/*
if(!in_array($_REQUEST['apiKey'], $apiKeys)){
    $response['code'] = "10006"; 
    $response['result'] = "Error"; 
    $response['message'] = "Invalid API Key was provided"; 
    echo json_encode($response);
    exit();    
}
*/


// Load base application
require_once 'thingEngine.php';


// CONNECT TO DB USING REDBEANPHP
try {
    \RedBeanPHP\Facade::setup( 'mysql:host='.$settings['dbserver'].';dbname='.$settings['dbname'],$settings['dbuser'], $settings['dbpass'] ); //for both mysql or mariaDB
    \RedBeanPHP\Facade::freeze( FALSE );
} catch (Exception $e) {
    echo "RedBeanPHP Could not connect to database, check your settings.";
    exit();
}

// INITIATE THINGENGINE
$thingEngine = new \thingEngine\Engine($settings);
$thingEngineApi = new \thingEngine\thingEngineApi($settings);

// Load base functions
require_once 'functions.php';

// Auto Load Plugins Available
require_once 'autoload.php';

