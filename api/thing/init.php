<?php
include_once("classes.php");
// API group
$thingEngine->slim->group("/".$thingEngine->settings['api_uri'], function () use ($thingEngine) {
    // Auth group
    $thingEngine->slim->group('/thing', function () use ($thingEngine) {
        // Home
        $thingEngine->slim->get('/', function () use ($thingEngine){
            echo "Generic Things";
        });
        
        // Get Person with ID
        $thingEngine->slim->get('/details/:id', function ($id) use ($thingEngine){
            $results['id'] = $id;
           
            /*
            $response = $thingEngine->slim->response();
            $response['Content-Type'] = 'application/json';
            $response['X-Powered-By'] = 'Potato Energy';
            $response->status(200);
            $response->body(json_encode($test));
            */
            echo json_encode($results);
        })->name("thingDetails");
    });
});