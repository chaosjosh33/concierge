<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../init.php';
$thingEngine->slim->config(array(
    'templates.path' => __DIR__
));
$thingEngine->slim->map('/', function () use ($thingEngine) {
    $thingEngine->slim->render('landingView.php');
})->via('GET','POST');
$thingEngine->slim->run();