<header id="myCarousel" class="carousel slide">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<div class="fill"><img src="theme/images/city.jpg" height="600px"></img></div>
		<div class="carousel-caption">
			<h2></h2>
		</div>
	</div>
	<div id="cardiv2"class="item">
		<div class="fill" ><img src="theme/images/family.jpg" height="600px"></div>
		<div class="carousel-caption">
			<h2></h2>
		</div>
	</div>
	<div id="cardiv3"class="item">
		<div class="fill" ><img src="theme/images/hills.jpg" height="600px"></div>
		<div class="carousel-caption">
			<h2></h2>
		</div>
	</div>
	</div>
<!-- Controls -->
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
	<span class="icon-prev"></span>
</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
	<span class="icon-next"></span>
</a>
</header>
<div class="container">
	<!-- Service Panels -->
	<!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">How we make your insurance better</h2>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<span class="fa-stack fa-5x">
						<i class="fa fa-circle fa-stack-2x text-primary"></i>
						<i class="fa fa-users fa-stack-1x fa-inverse"></i>
					</span>
				</div>
				<div class="panel-body">
					<h4>Insurance Experts</h4>
					<p>Let our insurance experts help you maximize what you're already paying for.</p>
					<a href="#" class="btn btn-primary">Learn More</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<span class="fa-stack fa-5x">
						<i class="fa fa-circle fa-stack-2x text-primary"></i>
						<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
					</span>
				</div>
				<div class="panel-body">
					<h4>Tele-Medicine</h4>
					<p>Why wait to see a doctor when you can talk to one by phone 24/7. </p>
					<a href="#" class="btn btn-primary">Learn More</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<span class="fa-stack fa-5x">
						<i class="fa fa-circle fa-stack-2x text-primary"></i>
						<i class="fa fa-file-text-o fa-stack-1x fa-inverse"></i>
					</span>
				</div>
				<div class="panel-body">
					<h4>Medical Bill Help</h4>
					<p>Why pay more than you have to. Let our medical bill experts verify the bill is correct and help negotiate a lower rate. </p>
					<a href="#" class="btn btn-primary">Learn More</a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="panel panel-default text-center">
				<div class="panel-heading">
					<span class="fa-stack fa-5x">
						<i class="fa fa-circle fa-stack-2x text-primary"></i>
						<i class="fa fa-dollar fa-stack-1x fa-inverse"></i>
					</span>
				</div>
				<div class="panel-body">
					<h4>Price Check</h4>
					<p>You shop for price on most everything you buy, why should health care and prescriptions be any different.</p>
					<a href="#" class="btn btn-primary">Learn More</a>
				</div>
			</div>
		</div>
	</div>

	<!-- Service List -->
	<!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Member services</h2>
		</div>
		<div class="col-md-4">
			<div class="media">
				<div class="pull-left">
					<span class="fa-stack fa-2x">
						<img src="theme/images/medical.png" class="img-responsive" width="60px">
					</span> 
				</div>
				<div class="media-body">
					<h4 class="media-heading">Medical </h4>
					<p>With no monthly membership fees, you can receive immediate discounts from over 410,000 nationwide physician offices and over 45,000 healthcare service providers.</p>
				</div>
			</div>
			<div class="media">
				<div class="pull-left">
					<span class="fa-stack fa-2x">
						<img src="theme/images/hearing.png" class="img-responsive" width="60px">
					</span> 
				</div>
				<div class="media-body">
					<h4 class="media-heading">Hearing</h4>
					<p>Save 30-60% on hearing aids through a national network of providers. You can also save on replacement batteries that are shipped directly to your home.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="media">
				<div class="pull-left">
					<span class="fa-stack fa-2x">
						<img src="theme/images/dental.png" class="img-responsive" width="60px">
					</span> 
				</div>
				<div class="media-body">
					<h4 class="media-heading">Dental</h4>
					<p>Available at over 400,000 dentists nationwide this plan offers discounts on dental care and other health-related services. </p>
				</div>
			</div>
			<div class="media">
				<div class="pull-left">
					<span class="fa-stack fa-2x">
						<img src="theme/images/lab.png" class="img-responsive" width="60px">
					</span> 
				</div>
				<div class="media-body">
					<h4 class="media-heading">Testing & Labs</h4>
					<p>Testing ordered by your doctor can be purchased with a cost savings of up to 50% less than a normal physician’s office or hospital.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="media">
				<div class="pull-left">
					<span class="fa-stack fa-2x">
						<img src="theme/images/vision.png" class="img-responsive" width="60px">
					</span> 
				</div>
				<div class="media-body">
					<h4 class="media-heading">Vision</h4>
					<p>Over 10,000 optical centers offer average discounts of 10% to 50% off their regular price for eyeglasses, contacts, lenses, sunglasses and corrective surgery including Lasik, RPK and more. </p>
				</div>
			</div>
			<div class="media">
				<div class="pull-left">
					<span class="fa-stack fa-2x">
						<img src="theme/images/pharmacy.png" class="img-responsive" width="60px">
					</span> 
				</div>
				<div class="media-body">
					<h4 class="media-heading">Pharmacy</h4>
					<p>The prescription discount program is an easy way to help you and your family save up to 85% on your prescription drug needs.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->
</div>
<script type="text/javascript">
	$(function() {
		$('.carousel').carousel({
			interval: 5000 //changes the speed
		})
	});
</script>