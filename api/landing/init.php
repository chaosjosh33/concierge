<?php
// API group
$thingEngine->slim->group("/".$thingEngine->settings['api_uri'], function () use ($thingEngine) {
    // Auth group
    $thingEngine->slim->group('/landing', function () use ($thingEngine) {
        // Home
        $thingEngine->slim->get('/', function () use ($thingEngine){
            echo "Landing Home";
        });
    });
});
