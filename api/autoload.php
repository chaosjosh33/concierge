<?php
class Autoloader {
    static public function loader($className) {
        try {
            $classNameParts = explode("\\",$className);
            $countIndex = count($classNameParts)-1;
            $dir = __DIR__ . "/". $classNameParts[$countIndex];
            
            if (is_dir($dir)) {
                if(file_exists($dir."/classes.php" )){
                    require_once $dir."/classes.php";
                    return TRUE;
                }
            }
            return FALSE;
        } catch (Exception $e) {
            $debugger[] = 'Autoloader Class = Caught exception: '.  $e->getMessage();
        }
    }
}
spl_autoload_register('Autoloader::loader');
// Add MemCache or session on list
$files = scan_Dir(__DIR__);

function scan_Dir($dir) {
    $arrfiles = array();
    if (is_dir($dir)) {
        if ($handle = opendir($dir)) {
            chdir($dir);
            while (false !== ($file = readdir($handle))) { 
                if ($file != "." && $file != "..") { 
                    if (is_dir($file)) { 
                        $arr = scan_Dir($file);
                        if(!empty($arr)){
                            foreach($arr as $key=>$var){
                                $arrfiles[] = $var;
                            }
                        }
                    } else {
                        $pos = strpos($file, "init.php");
                        if ($pos !== false) {
                            $arrfiles[] = $dir."/".$file;
                        }
                        $pos2 = strpos($file, "classes.php");
                        if ($pos2 !== false) {
                           // $arrfiles[] = $dir."/".$file;
                        }
                    }
                }
            }
            chdir("../");
        }
        closedir($handle);
    }
    return $arrfiles;
}
// Loop through files to require
if(!empty($files)){
    if(is_array($files)){
        foreach($files as $key=>$var){
            try {
                require_once $var;
            } catch (Exception $e) {
                $debugger[] = 'autoload.php = Caught exception: '.  $e->getMessage();
            }
        }
    }
}