<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Insurance Concierge</title>
		<!-- Bootstrap Core CSS -->
		<link href="<?php echo $settings['base_uri'];?>theme/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="<?php echo $settings['base_uri'];?>theme/css/modern-business.css" rel="stylesheet">
		<!-- Custom Fonts -->
		<link href="<?php echo $settings['base_uri'];?>theme/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo $settings['base_uri'];?>theme/css/bootstrap-formhelpers-min.css" media="screen">
		<link rel="stylesheet" href="<?php echo $settings['base_uri'];?>theme/css/bootstrapValidator-min.css"/>
		<script>
			var serialize = function(obj) {
				var str = [];
				for(var p in obj)
					if (obj.hasOwnProperty(p)) {
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					}
				return str.join("&");
			};
			var base_uri = "<?php echo $settings['base_uri'];?>";
			var api_uri = "<?php echo $settings['api_uri'];?>";
			var userData = <?php echo json_encode($userData);?>;
		</script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	</head>
	<body>
		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="<?php echo $settings['base_uri'];?>"><img class="img-responsive" width="400px" style="padding:20px" src="<?php echo $settings['base_uri'];?>theme/images/logo.png"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<?php /*?>
						<li>
							<a href="#content/contact">Contact</a>
						</li>
						<?php */?>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<div id="content">
			<script src="<?php echo $settings['base_uri'];?>theme/js/bootstrap-formhelpers-min.js"></script>
			<form action="<?php echo $settings['base_uri'];?>api/products/charge" method="POST" id="payment-form" class="form-horizontal">
				<div style="margin-top:200px" class="row row-centered">
					<div class="col-md-4 col-md-offset-4">
						<div class="alert alert-danger" id="a_x200" style="display: none;"> <strong>Error!</strong> <span class="payment-errors"></span> </div>
						<span class="payment-success">
							<?php echo $success ?>
							<?php echo $error ?>
							<p class="text-center"><a href="<?php echo $settings['base_uri'];?>">Home</a></p>
						</span>
							</form>
						<script type="text/javascript">
							$(document).ready(function() {
								$('#payment-form').bootstrapValidator({
									message: 'This value is not valid',
									feedbackIcons: {
										valid: 'glyphicon glyphicon-ok',
										invalid: 'glyphicon glyphicon-remove',
										validating: 'glyphicon glyphicon-refresh'
									},
									submitHandler: function(validator, form, submitButton) {
										// createToken returns immediately - the supplied callback submits the form if there are no errors
										Stripe.card.createToken({
											number: $('.card-number').val(),
											cvc: $('.card-cvc').val(),
											exp_month: $('.card-expiry-month').val(),
											exp_year: $('.card-expiry-year').val(),
											name: $('.card-holder-name').val(),
											address_line1: $('.address').val(),
											address_city: $('.city').val(),
											address_zip: $('.zip').val(),
											address_state: $('.state').val(),
											address_country: $('.country').val()
										}, stripeResponseHandler);
										return false; // submit from callback
									},
									fields: {
										street: {
											validators: {
												notEmpty: {
													message: 'The street is required and cannot be empty'
												},
												stringLength: {
													min: 6,
													max: 96,
													message: 'The street must be more than 6 and less than 96 characters long'
												}
											}
										},
										city: {
											validators: {
												notEmpty: {
													message: 'The city is required and cannot be empty'
												}
											}
										},
										zip: {
											validators: {
												notEmpty: {
													message: 'The zip is required and cannot be empty'
												},
												stringLength: {
													min: 3,
													max: 9,
													message: 'The zip must be more than 3 and less than 9 characters long'
												}
											}
										},
										email: {
											validators: {
												notEmpty: {
													message: 'The email address is required and can\'t be empty'
												},
												emailAddress: {
													message: 'The input is not a valid email address'
												},
												stringLength: {
													min: 6,
													max: 65,
													message: 'The email must be more than 6 and less than 65 characters long'
												}
											}
										},
										cardholdername: {
											validators: {
												notEmpty: {
													message: 'The card holder name is required and can\'t be empty'
												},
												stringLength: {
													min: 6,
													max: 70,
													message: 'The card holder name must be more than 6 and less than 70 characters long'
												}
											}
										},
										cardnumber: {
											selector: '#cardnumber',
											validators: {
												notEmpty: {
													message: 'The credit card number is required and can\'t be empty'
												},
												creditCard: {
													message: 'The credit card number is invalid'
												},
											}
										},
										expMonth: {
											selector: '[data-stripe="exp-month"]',
											validators: {
												notEmpty: {
													message: 'The expiration month is required'
												},
												digits: {
													message: 'The expiration month can contain digits only'
												},
												callback: {
													message: 'Expired',
													callback: function(value, validator) {
														value = parseInt(value, 10);
														var year         = validator.getFieldElements('expYear').val(),
															currentMonth = new Date().getMonth() + 1,
															currentYear  = new Date().getFullYear();
														if (value < 0 || value > 12) {
															return false;
														}
														if (year == '') {
															return true;
														}
														year = parseInt(year, 10);
														if (year > currentYear || (year == currentYear && value > currentMonth)) {
															validator.updateStatus('expYear', 'VALID');
															return true;
														} else {
															return false;
														}
													}
												}
											}
										},
										expYear: {
											selector: '[data-stripe="exp-year"]',
											validators: {
												notEmpty: {
													message: 'The expiration year is required'
												},
												digits: {
													message: 'The expiration year can contain digits only'
												},
												callback: {
													message: 'Expired',
													callback: function(value, validator) {
														value = parseInt(value, 10);
														var month        = validator.getFieldElements('expMonth').val(),
															currentMonth = new Date().getMonth() + 1,
															currentYear  = new Date().getFullYear();
														if (value < currentYear || value > currentYear + 100) {
															return false;
														}
														if (month == '') {
															return false;
														}
														month = parseInt(month, 10);
														if (value > currentYear || (value == currentYear && month > currentMonth)) {
															validator.updateStatus('expMonth', 'VALID');
															return true;
														} else {
															return false;
														}
													}
												}
											}
										},
									}
								});
							});
						</script>
						<script type="text/javascript">
							// this identifies your website in the createToken call below
							Stripe.setPublishableKey('<?php echo $settings['stripe']['test']['publickey'];?>');

							function stripeResponseHandler(status, response) {
								if (response.error) {
									// re-enable the submit button
									$('.submit-button').removeAttr("disabled");
									// show hidden div
									document.getElementById('a_x200').style.display = 'block';
									// show the errors on the form
									$(".payment-errors").html(response.error.message);
								} else {
									var form$ = $("#payment-form");
									// token contains id, last4, and card type
									var token = response['id'];
									// insert the token into the form so it gets submitted to the server
									form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
									// and submit
									form$.get(0).submit();
								}
							}


						</script>
					</div>
					<!-- Page Content -->
					<!-- /.container -->
					<!-- jQuery -->
					<link rel="icon" href="<?php echo $settings['base_uri'];?>theme/images/favicon.ico?ver=4" />
					<script src="<?php echo $settings['base_uri'];?>theme/js/jquery.js"></script>
					<script src="<?php echo $settings['base_uri'];?>theme/js/vendor/jquery-ui/jquery-ui.min.js"></script>
					<!-- Bootstrap Core JavaScript -->
					<script src="<?php echo $settings['base_uri'];?>theme/js/bootstrap.min.js"></script>
					<script src="https://js.stripe.com/v2/"></script>
					<script src="<?php echo $settings['base_uri'];?>theme/js/bootstrap-formhelpers-min.js"></script>
					<script src="<?php echo $settings['base_uri'];?>theme/js/bootstrapValidator-min.js"></script>
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content" id="modalContent">
							</div>
						</div>
					</div>
					<!-- Script to Activate the Carousel -->
					<script>
						$(function(){
							$('#buy').on('click', function(){
								$( "#modalContent" ).load( base_uri + "api/products/productList.php", function(){
								});
							});
						});
					</script>
					</body>
				</html>
