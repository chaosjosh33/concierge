<?php
namespace thingEngine;
// Event EXTENSION
class products extends \thingEngine\thingExtension{
	protected $address;
	protected $city;
	protected $state;
	protected $zip;
	protected $brand;
	protected $last4;
	protected $country;
	protected $email;
	protected $plan;
	protected $cardholderName;
	protected $customer;
	protected $setupFee;
	function __construct($settings = false){
		$this->extensionInfo['name'] = "Charge Extension";
		$this->extensionInfo['description'] = "Basic Charge Structure";
		$this->extensionInfo['author'] = "ThingEngine";
		$this->extensionInfo['owner'] = "thingengine";
		$this->extensionInfo['price'] = "0.00";
		$this->extensionInfo['version'] = "1.0";
		$this->extensionInfo['dateCreated'] = "12/01/2015";
	}
	public function saveCharge(){
		$this->saveData("charges");
	}
}