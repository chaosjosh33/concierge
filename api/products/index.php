<?php
	ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
require_once __DIR__ . '/../init.php';
$thingEngine->slim->config(array(
	'templates.path' => __DIR__
));
$thingEngine->slim->map('/', function () use ($thingEngine) {
})->via('GET','POST');
$thingEngine->slim->map('/list', function () use ($thingEngine) {
	$thingEngine->slim->render('productList.php');
})->via('GET','POST');
$thingEngine->slim->map('/ac', function () use ($thingEngine,$settings) {
	$charge  = new \thingEngine\products($settings);
	$charge->address = 'preston';
	$charge->city = 'fontana';
	$charge->zip = 'zip';
	$charge->country = 'us';
	$charge->brand = 'visa';
	$charge->last4 = 'visa';
	$charge->email = 'email';
	$charge->state = 'state';
	$charge->cardholderName = 'name';
	$charge->plan ='plan';
	$charge->customer = 'customer';
	$charge->saveCharge();
	echo '<pre>';print_r($charge);
})->via('GET','POST');
$thingEngine->slim->map('/charge', function () use ($thingEngine,$settings) {
	$charge  = new \thingEngine\products($settings);
	$error = '';
	$success = '';
	$amount = '1500';
	if ($_POST) {
		\Stripe\Stripe::setApiKey($settings['stripe']['live']['secretkey']);
		$error = '';
		$success = '';
		try {
			if (empty($_POST['street']) || empty($_POST['city']) || empty($_POST['zip'])){
				throw new Exception("Fill out all required fields.");
			}
			if (!isset($_POST['stripeToken'])){
				throw new Exception("The Stripe Token was not generated correctly");
			}
			$customer= \Stripe\Customer::create(array(
				"description" => $_POST['email'],
				"source" => $_POST['stripeToken'] // obtained with Stripe.js
			));
			$setup='yes';
			if((!empty($_POST['coupon']))&&(strtoupper($_POST['coupon'])=='NOSETUP356')){
				$setup ='no';
			}
			if($setup=='yes'){
				$ccCharge = \Stripe\Charge::create(array(
					"amount" => $amount,
					"currency" => "usd",
					"customer" => $customer['id'],
					"description" => $_POST['email']
				));
			}
			$subscribe= \Stripe\Subscription::create(array(
				"customer" => $customer['id'],
				"plan" => $_POST['plan']
			));
			$charge->address = $customer['sources']['data'][0]['address_line1'];
			$charge->city = $customer['sources']['data'][0]['address_city'];
			$charge->zip = $customer['sources']['data'][0]['address_zip'];
			$charge->country = $customer['sources']['data'][0]['country'];
			$charge->brand = $customer['sources']['data'][0]['brand'];
			$charge->last4 = $customer['sources']['data'][0]['last4'];
			$charge->email = $customer['description'];
			$charge->state = $customer['sources']['data'][0]['address_state'];
			$charge->cardholderName = $_POST['cardholdername'];
			$charge->plan = $_POST['plan'];
			$charge->customer = $customer['id'];
			$charge->setupFee = $setup;
			$charge->saveCharge();
			$success = '<div class="alert alert-success">
                <strong>Success!</strong> Your payment was successful.
				</div>';
		}
		catch (Exception $e) {
			$error = '<div class="alert alert-danger">
			  <strong>Error!</strong> '.$e->getMessage().'
			  </div>';
		}
	}
	$thingEngine->slim->render('chargePage.php', array('settings'=>$settings,'error'=>$error,'success'=>$success, 'amount'=>$amount, 'product'=>$product));
})->via('GET','POST');
$thingEngine->slim->map('/buy/:product', function ($product) use ($thingEngine,$settings) {
	$error = '';
	$success = '';
	$amount = '15.00';
	$thingEngine->slim->render('buyPage.php', array('settings'=>$settings,'error'=>$error,'success'=>$success, 'amount'=>$amount, 'product'=>$product));
})->via('GET','POST');
$thingEngine->slim->run();