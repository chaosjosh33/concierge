<div class="container">
	<br>
	<div class="col-md-3 col-sm-6">
		<div class="panel panel-default text-center">
			<div class="panel-heading">
				<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-user fa-stack-1x fa-inverse"></i>
				</span>
			</div>
			<div class="panel-body">
				<h4>Individual</h4>
				<h3>$9.99/month</h3>
				<h3 style="margin-bottom:0">$14.95/month</h3>
				<p>$0 Copay</p>
				<a  product="1001"  class="BuyBtn btn btn-primary">Buy</a>
				<a  product="1005"  class="BuyBtn btn btn-primary">Buy $0 Copay</a>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6">
		<div class="panel panel-default text-center">
			<div class="panel-heading">
				<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-child fa-stack-1x fa-inverse"></i>
				</span>
			</div>
			<div class="panel-body">
				<h4>Idividual + Child(ren)</h4>
				<h3>$14.99/month</h3>
				<h3 style="margin-bottom:0">$24.95/month</h3>
				<p>$0 Copay</p>
				<a  product="1002"  class="BuyBtn btn btn-primary">Buy</a>
				<a  product="1006"  class="BuyBtn btn btn-primary">Buy $0 Copay</a>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6">
		<div class="panel panel-default text-center">
			<div class="panel-heading">
				<span class="fa-stack fa-5x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-users fa-stack-1x fa-inverse"></i>
				</span>
			</div>
			<div class="panel-body">
				<h4>Family Plan</h4>
				<h3>$19.99/month</h3>
				<h3 style="margin-bottom:0">$34.95/month</h3>
				<p>$0 Copay</p>
				<a  product="1003"  class="BuyBtn btn btn-primary">Buy</a>
				<a  product="1007"  class="BuyBtn btn btn-primary">Buy $0 Copay</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.BuyBtn').on('click', function(){
			window.location.href='#content/products/buy/'+$(this).attr('product');
			$('#myModal').modal('toggle');
		});
	});
</script>