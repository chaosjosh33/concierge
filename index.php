<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
// INCLUDE ALL APP FILES 
require 'api/init.php';
use Firebase\Token\TokenException;
use Firebase\Token\TokenGenerator;  
/************************************************************
*
*   Create Routes custom to your own application here
*   ThingEngine Routes will happen when the $settings['api_uri'] in the thingEngine/config.php
*   file is set.  Example: if you have it set to "api" then yourdomain.com/api
*   will go into the thingEngine directory and look for routes and bypass below routes. 
************************************************/
$thingEngine->slim->get('/', $authLoggedIn, function () use ($thingEngine, $settings){
if(!empty($_SESSION['api']['user'])){
    $userData = $_SESSION['api']['user'];
}else{
	$userData=array();
}
    try {
        $generator = new TokenGenerator($settings['firebaseSecret']);
        $token = $generator
            ->setData(array('uid' => 'test'))
            ->setOption('admin', true)
            ->create();
    } catch (TokenException $e) {
        $token = "Error: ".$e->getMessage();
    }
    $view = $thingEngine->slim->view();
    $view->setTemplatesDirectory(__DIR__.'/theme/');
    $rendered_template = $view->fetch('index.php', array('thingEngine'=>$thingEngine, 'settings'=>$settings, 'userData'=>$userData, 'token'=>$token));
    echo $rendered_template;
    //debug($_SESSION['slim.flash']['error']);
});
$thingEngine->slim->run();